"use strict";

const Color = {UNDEFINED: -1, YELLOW: 0, GREEN: 1, BLUE: 2, RED: 3, ORANGE: 4, WHITE: 5, PURPLE: 6, EMPTY: 7, PAWN: 8};

function is_color(color) {
    switch (color) {
        case Color.YELLOW:
            return true;
        case Color.GREEN:
            return true;
        case Color.BLUE:
            return true;
        case Color.RED:
            return true;
        case Color.ORANGE:
            return true;
        case Color.WHITE:
            return true;
        case Color.PURPLE:
            return true;
        case Color.EMPTY:
            return false;
        case Color.PAWN:
            return false;
        case Color.UNDEFINED:
            return false;
        default:
            console.log('Error to read value in isColor function');
            return false;
    }
}

function convert_to_hexa(color) {
    switch (color) {
        case Color.YELLOW:
            return "#EFEF2D";
        case Color.GREEN:
            return "#00DB16";
        case Color.BLUE:
            return "#5B93E6";
        case Color.RED:
            return "#EC1919";
        case Color.ORANGE:
            return "#FF8C00";
        case Color.WHITE:
            return "#FFFFFF";
        case Color.PURPLE:
            return "#800080";
        case Color.EMPTY:
            return "#A47940";
        case Color.PAWN:
            return "#000000";
        case Color.UNDEFINED:
            return "#A47940";
        default:
            console.log('Error to read value in convert_to_hexa function');
            return false;
    }
}

export {convert_to_hexa, is_color};
export default Color;
