"use strict";

import State from "./state.mjs";

class Piece {
    constructor(c, t) {
        this._color = c;
        this._type = t;
    }

// public methods
    clone() {
        return new Piece(this._color, this._type);
    }

    color() {
        return this._color;
    }

    type() {
        return this._type;
    }
}

export default Piece;
