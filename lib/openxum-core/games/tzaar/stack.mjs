"use strict";

import State from "./state.mjs";

class Stack {
    constructor() {
        this._pieces = [];
    }

// public methods
    color() {
        return this._top().color();
    }

    clear() {
        while (!this.empty()) {
            this._pieces.pop();
        }
    }

    clone() {
        let o = new Stack();

        for (let i = 0; i < this._pieces.length; ++i) {
            o.put_piece(this._pieces[i].clone());
        }
        return o;
    }

    empty() {
        return this._pieces.length === 0;
    }

    put_piece(piece) {
        this._pieces.push(piece);
    }

    remove_top() {
        const _top = this._top();

        this._pieces.pop();
        return _top;
    }

    size() {
        return this._pieces.length;
    }

    type() {
        return this._top().type();
    }

// private attributes
    _top() {
        return this._pieces[this._pieces.length - 1];
    }
}

export default Stack;
