// lib/openxum-core/games/newgame/engine.mjs

import OpenXum from '../../openxum/index.mjs';
import MoveType from './move_type.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Move from './move.mjs';


class Engine extends OpenXum.Engine {
    constructor(t, c) {
        super();
        this._type = t;
        this._current_color = c;
        this._white_stones = 84;
        this._blue_stones = 84;
        this._turn = 1;
        this._player = 1;
        this._last_move = null;
        this._gameBoard = new Array(13);
        for (let i = 0; i < 13; i++) {
            this._gameBoard[i] = new Array(13);
            for (let j = 0; j < 13; j++) {
                this._gameBoard[i][j] = Color.NONE;
            }
        }


    }

    build_move() {
        return new Move();
    }

    clone() {
        let o = new Engine(this._type, this._current_color);

        o._attribute = this._attribute;

        return o;
    }


    current_color() {
        return this._current_color;
    }


    get_name() {
        return 'yavalax';
    }


    get_possible_move_list() {
        let moves_list = [];
        for (let i = 0; i < 13; i++) {
            for (let j = 0; j < 13; j++) {
                let cord = new Coordinates(i, j);

                if (cord.is_valid() && !this.is_finished() && this._gameBoard[i][j] === Color.NONE && !this.check_winning(cord).hasAlignment) {
                    moves_list.push(new Move(MoveType.PUT_PIECE, this.current_color(), cord));
                }
            }
        }
        return moves_list;
    }

    line_length(cord, play_horizontal, play_vertical) {
        let line_length = 1;
        let newCord = cord.clone();

        newCord._line += play_vertical;
        newCord._column += play_horizontal;

        while (
            newCord.is_valid() &&
            this._gameBoard[newCord._line][newCord._column] === this.current_color()
            ) {
            line_length++;
            newCord._line += play_vertical;
            newCord._column += play_horizontal;
        }

        return line_length;
    }

    check_winning(cord) {
        const winningDirections = [
            {play_horizontal: 1, play_vertical: 0},
            {play_horizontal: 0, play_vertical: 1},
            {play_horizontal: 1, play_vertical: 1},
            {play_horizontal: -1, play_vertical: 1},
            {play_horizontal: -1, play_vertical: -1},
            {play_horizontal: 0, play_vertical: -1 },
            {play_horizontal: -1, play_vertical: 0},
            {play_horizontal: 1, play_vertical: -1}
        ];

        let alignCount = 0;

        for (const direction of winningDirections) {
            const lineLength = this.line_length(cord, direction.play_horizontal, direction.play_vertical);

            if (lineLength === 4) {
                alignCount++;
            }
        }

        return { hasAlignment: alignCount >= 2, alignmentCount: alignCount};
    }



    is_finished() {
        if (this.winner_is === 0 || this.winner_is() === 1 || this.winner_is() === 2) {
            return true;
        } else {
            return false;
        }
    }

    _updateTurnAndPlayer() {
        this._player++;
        this._turn++;

    }

    _put_piece(cord) {

        if (this.current_color() === Color.WHITE && cord.is_valid()
            && this._gameBoard[cord._line][cord._column] === Color.NONE) {

            this._gameBoard[cord._line][cord._column] = this.current_color();
            this._white_stones--;
            this._last_move = new Move(MoveType.PUT_PIECE, this.current_color(), cord);
            this._updateTurnAndPlayer();
        } else if (this.current_color() === Color.BLUE && cord.is_valid()
            && this._gameBoard[cord._line][cord._column] === Color.NONE) {

            this._gameBoard[cord._line][cord._column] = this.current_color();
            this._blue_stones--;
            this._last_move = new Move(MoveType.PUT_PIECE, this.current_color(), cord);
            this._updateTurnAndPlayer();
        }
    }


    move(move) {
        if (move._type === MoveType.PUT_PIECE) {
            this._put_piece(move._coordinates);

            if (!this.is_finished()) {

                if (this._turn > 2 && this.current_color() === Color.WHITE) {
                    this._current_color = Color.BLUE;
                    this._turn = 1;
                } else if (this._turn > 2 && this.current_color() === Color.BLUE) {
                    this._current_color = Color.WHITE;
                    this._turn = 1;
                }


            }
        }
    }

    parse(str) {
        // TODO
    }

    to_string() {
        // TODO
    }

    winner_is() {
        if (this._blue_stones === 0 && this._white_stones === 0) {
            return Color.EQUAL;
        } else if (this._player === 1) {
            return Color.NONE;
        }

        const winningResult = this.check_winning(this._last_move._coordinates);
        if (winningResult.hasAlignment) {
            const winningCoord = this._last_move._coordinates;
            return this._gameBoard[winningCoord._line][winningCoord._column];
        }

        return Color.NONE;
    }


}

export default Engine
