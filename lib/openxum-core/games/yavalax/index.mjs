import GameType from './game_type.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import Engine from './engine.mjs';
import Coordinates from "./coordinates.mjs";
import Color from "./color.mjs";

//import Phase from './phase.mjs';
// import ...

export default {
    Engine: Engine,
    GameType: GameType,
    Move: Move,
    MoveType: MoveType,
    Coordinates: Coordinates,
    Color:Color
}