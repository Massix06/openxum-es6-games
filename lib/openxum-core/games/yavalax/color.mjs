"use strict";

const Color = {NONE: -1, WHITE: 0, BLUE: 1,EQUAL: 2}; //EQUAL POUR UN JEU NUL

export default Color;

// we don't need to use phase file cause our stones are fix and don't move
