"use strict";

class Coordinates {
    constructor(l, c) {
        this._column = c;
        this._line = l;
    }
    is_valid() {
        if(this._line >= 0 && this._line < 13 && this._column >= 0 && this._column < 13){
            return true;
        }else {
            return false;
        }
    }

    clone() {
        return new Coordinates(this._line, this._column);
    }

    equals(coordinates) {
        return this._column === coordinates.get_column() &&
            this._line === coordinates.get_line();
    }

    get_column() {
        return this._column;
    }

    get_line() {
        return this._line;
    }


    to_string() {
        if (!this.is_valid()) {
            return "Invalid coordinates";
        }
        return "Column:"+String(this._column)+"line:"+ String(this._line);
    }

}
export default Coordinates;