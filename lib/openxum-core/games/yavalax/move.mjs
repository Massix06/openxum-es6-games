"use strict";


import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs'
import Coordinates from './coordinates.mjs';


class Move extends OpenXum.Move {
    constructor(t, c, cord) {
        super();
        this._type = t;
        this._color = c;
        this._coordinates = cord;


    }

// public methods
    decode(str) {
        this._type = MoveType.PUT_PIECE; // str[0]
        if (str[1] === 'W'){
            this._color = Color.WHITE;
        }else if (str[1] === 'B'){
            this._color = Color.BLUE;
        }else if (str[1] === 'N'){
            this._color = Color.NONE;
        }else {
            this._color = Color.EQUAL;
        }
        this._coordinates = new Coordinates(parseInt(str[2]),parseInt(str[3]));

    }
    encode() {
        if (this._type === MoveType.PUT_PIECE) {
            let str = "P";
            if (this._color === Color.WHITE){
                str += "W";
            }else if (this._color === Color.BLUE){
                str += "B";
            }else if(this._color === Color.NONE){
                str += "N";
            }else{
                str += "E";
            }
            return str+String(this._coordinates._line)+String(this._coordinates._column);
        }

    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._coordinates = new Coordinates(data.coordinate.l, data.coordinate.c);
    }


    to_object() {
        return {type: this._type, color: this._color, coordinate: this._coordinate};
    }

    to_string() {
        if (this._type === MoveType.PUT_PIECE) {
            let str = "Put";
            if (this._color === Color.WHITE){
                str += "W";
            }else if (this._color === Color.BLUE){
                str += "B";
            }else if(this._color === Color.NONE){
                str += "N";
            }else{
                str += "E";
            }
            return str +" "+ "piece at" +" "+ String(this._coordinates._line) +" "+ String(this._coordinates._column);
        }

    }

    get_type() {
        return this._type;
    }
}

export default Move;

