"use strict";

import Board from './board.mjs';
import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import Direction from './direction.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
    constructor(t, c, p1, p2) {
        super();
        this._type = t;
        this._color = c;
        if (this._type === MoveType.PUT_MARBLE) {
            this._coordinates = p1;
        } else if (this._type === MoveType.ROTATE) {
            this._board = p1;
            this._direction = p2;
        }
    }

// public methods
    board() {
        return this._board;
    }

    color() {
        return this._color;
    }

    coordinates() {
        return this._coordinates;
    }

    decode(str) {
        const type = str.charAt(0);

        if (type === 'P') {
            this._type = MoveType.PUT_MARBLE;
        } else if (type === 'R') {
            this._type = MoveType.ROTATE;
        }
        this._color = str.charAt(1) === 'B' ? Color.BLACK : Color.WHITE;
        if (this._type === MoveType.PUT_MARBLE) {
            this._coordinates = new Coordinates(str.charAt(2), parseInt(str.charAt(3)));
        } else if (this._type === MoveType.ROTATE) {
            const board = str.substring(2, 4);

            if (board === 'bl') {
                this._board = Board.BOTTOM_LEFT;
            } else if (board === 'br') {
                this._board = Board.BOTTOM_RIGHT;
            } else if (board === 'tl') {
                this._board = Board.TOP_LEFT;
            } else { // board === 'tr'
                this._board = Board.TOP_RIGHT;
            }
            if (str.charAt(4) === 'c') {
                this._direction = Direction.CLOCKWISE;
            } else {
                this._direction = Direction.ANTI_CLOCKWISE;
            }
        }
    }

    direction() {
        return this._direction;
    }

    encode() {
        if (this._type === MoveType.PUT_MARBLE) {
            return 'P' + (this._color === Color.BLACK ? "B" : "W") + this._coordinates.to_string();
        } else if (this._type === MoveType.ROTATE) {
            return 'R' + (this._color === Color.BLACK ? "B" : "W") +
                (this._board === Board.BOTTOM_LEFT ? 'bl' : this._board === Board.BOTTOM_RIGHT ? 'br' : this._board === Board.TOP_LEFT ? 'tl' : 'tr') +
                (this._direction === Direction.CLOCKWISE ? 'c' : 'a');
        }
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._coordinates = new Coordinates(data.coordinates.letter, data.coordinates.number);
        this._board = data.board;
        this._direction = data.direction;
    }

    to_object() {
        return {
            type: this._type,
            color: this._color,
            coordinates: this._coordinates === null ? {letter: -1, number: -1} : {
                letter: this._coordinates._letter,
                number: this._coordinates._number
            },
            board: this._board,
            direction: this._direction
        };
    }

    to_string() {
        if (this._type === MoveType.PUT_MARBLE) {
            return 'put ' + (this._color === Color.BLACK ? "black" : "white") + ' marble to ' + this._coordinates.to_string();
        } else if (this._type === MoveType.ROTATE) {
            return 'rotate ' +
                (this._board === Board.BOTTOM_LEFT ? 'bottom left' : this._board === Board.BOTTOM_RIGHT ? 'bottom right' : this._board === Board.TOP_LEFT ? 'top left' : 'top right') +
                ' board with ' +
                (this._direction === Direction.CLOCKWISE ? 'clockwise' : 'anticlockwise') + ' direction';
        }
    }

    type() {
        return this._type;
    }
}

export default Move;
