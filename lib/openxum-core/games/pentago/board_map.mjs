"use strict";

import Board from "./board.mjs";

let BoardMap = {
    'tl': Board.TOP_LEFT,
    'tr': Board.TOP_RIGHT,
    'bl': Board.BOTTOM_LEFT,
    'br': Board.BOTTOM_RIGHT
};

export default BoardMap;
