"use strict";

import Board from "./board.mjs";
import Color from './color.mjs';
import Direction from './direction.mjs';
import Coordinates from './coordinates.mjs';
import GameType from './game_type.mjs';
import Move from './move.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Phase from './phase.mjs';
import Pivot from './pivot.mjs';
import State from './state.mjs';

class Engine extends OpenXum.Engine {
    constructor(t, c) {
        super();
        this._type = t;
        this._current_color = c;
        this._marble_number = 0;
        this._phase = Phase.PUT_MARBLE;
        this._state = [];
        for (let c = 0; c < 6; ++c) {
            let column = [];
            for (let l = 0; l < 6; ++l) {
                column.push(State.EMPTY);
            }
            this._state.push(column);
        }
    }

// public methods
    build_move() {
        return new Move();
    }

    clone() {
        let o = new Engine(this._type, this._current_color);

        o._set(this._marble_number, this._state, this._phase);
        return o;
    }

    current_color() {
        return this._current_color;
    }

    get_free_cells() {
        let list = [];

        for (let l = 0; l < 6; ++l) {
            for (let c = 0; c < 6; ++c) {
                if (this._state[c][l] === State.EMPTY) {
                    list.push(new Coordinates(String.fromCharCode('a'.charCodeAt(0) + c), l + 1));
                }
            }
        }
        return list;
    }

    get_name() {
        return 'pentago';
    }

    get_possible_move_list() {
        let list = [];

        if (this._phase === Phase.PUT_MARBLE) {
            const free_cells = this.get_free_cells();

            free_cells.forEach((c) => {
                list.push(new Move(MoveType.PUT_MARBLE, this._current_color, c));
            });
        } else if (this._phase === Phase.ROTATE) {
            Object.keys(Board).map((b, i) => {
                Object.keys(Direction).map((d, j) => {
                    list.push(new Move(MoveType.ROTATE, this._current_color, Board[b], Direction[d]));
                });
            });
        }
        return list;
    }

    get_state(coordinates) {
        var c = this._parse_coordinates(coordinates);

        return this._state[c.c][c.l];
    }

    get_type() {
        return this._type;
    }

    is_finished() {
        return this._check_alignment(Color.BLACK) || this._check_alignment(Color.WHITE) || this._marble_number === 36;
    }

    move(move) {
        if (move.type() === MoveType.PUT_MARBLE) {
            this._put_marble(move.coordinates().to_string(), move.color());
        } else if (move.type() === MoveType.ROTATE) {
            this._rotate(move.board(), move.direction());
        }
    }

    parse(str) {
        // TODO
    }

    phase() {
        return this._phase;
    }

    to_string() {
        // TODO
    }

    winner_is() {
        return this._check_alignment(Color.BLACK) ? Color.BLACK : (this._check_alignment(Color.WHITE) ? Color.WHITE : null);
    }

// private methods
    _change_color() {
        this._current_color = this._current_color === Color.BLACK ? Color.WHITE : Color.BLACK;
    }

    _build_alignment(c, l, a, s) {
        if (this._state[c][l] === s) {
            if (a.n > 0) {
                ++a.n;
            } else {
                a.n = 1;
            }
        } else {
            a.found = a.n >= 5;
            a.n = 0;
        }
        return a;
    }

    _check_alignment(color) {
        const s = color === Color.BLACK ? State.BLACK : State.WHITE;

        if (!this._check_horizontal_alignment(s)) {
            if (!this._check_vertical_alignment(s)) {
                return this._check_diagonal_alignment(s);
            }
        }
        return true;
    }

    _check_left_right_diagonal_alignment(s) {
        let a = {n: 0, found: false};

        for (let k = -1; k < 2; ++k) {
            let l, c;

            for (l = k < 0 ? 1 : 0, c = k > 0 ? 1 : 0; l < 6 && c < 6 && !a.found; ++l, ++c) {
                a = this._build_alignment(c, l, a, s);
            }
            a.n = 0;
        }
        return a.found;
    }

    _check_right_left_diagonal_alignment(s) {
        let a = {n: 0, found: false};

        for (let k = -1; k < 2; ++k) {
            let l, c;

            for (l = k < 0 ? 1 : 0, c = k > 0 ? 4 : 5; l >= 0 && c >= 0 && !a.found; --l, --c) {
                a = this._build_alignment(c, l, a, s);
            }
            a.n = 0;
        }
        return a.found;
    }

    _check_diagonal_alignment(s) {
        return this._check_left_right_diagonal_alignment(s) || this._check_right_left_diagonal_alignment(s);
    }

    _check_horizontal_alignment(s) {
        let a = {n: 0, found: false};

        for (let l = 0; l < 6 && !a.found; ++l) {
            for (let c = 0; c < 6 && !a.found; ++c) {
                a = this._build_alignment(c, l, a, s);
            }
            a.n = 0;
        }
        return a.found;
    }

    _check_vertical_alignment(s) {
        let a = {n: 0, found: false};

        for (let c = 0; c < 6 && !a.found; ++c) {
            for (let l = 0; l < 6 && !a.found; ++l) {
                a = this._build_alignment(c, l, a, s);
            }
            a.n = 0;
        }
        return a.found;
    }

    _copy_board(pivot, new_state) {
        for (let l = 0; l < 3; ++l) {
            for (let c = 0; c < 3; ++c) {
                this._state[pivot.c + c][pivot.l + l] = new_state[c][l];
            }
        }
    }

    _get_pivot(board) {
        return Pivot[board];
    }

    _init_empty_board() {
        return [
            [State.EMPTY, State.EMPTY, State.EMPTY],
            [State.EMPTY, State.EMPTY, State.EMPTY],
            [State.EMPTY, State.EMPTY, State.EMPTY]
        ];
    }

    _parse_coordinates(coordinates) {
        return {
            c: coordinates.charCodeAt(0) - 'a'.charCodeAt(0),
            l: coordinates.charCodeAt(1) - '1'.charCodeAt(0)
        };
    }

    _put_marble(coordinates, color) {
        const c = this._parse_coordinates(coordinates);

        if (this._state[c.c][c.l] === State.EMPTY) {
            this._state[c.c][c.l] = color === Color.BLACK ? State.BLACK : State.WHITE;
            this._marble_number++;
            this._phase = Phase.ROTATE;
        } else {
            throw new Error("InvalidMove");
        }
    }

    _rotate(board, direction) {
        const pivot = this._get_pivot(board);

        if (direction === Direction.CLOCKWISE) {
            this._copy_board(pivot, this._rotate_clockwise(pivot));
        } else {
            this._copy_board(pivot, this._rotate_anticlockwise(pivot));
        }
        this._phase = Phase.PUT_MARBLE;
        this._change_color();
    }

    _rotate_clockwise(pivot) {
        let new_state = this._init_empty_board();

        for (let index = 0; index < 3; ++index) {
            new_state[0][index] = this._state[pivot.c + index][pivot.l + 2];
            new_state[1][index] = this._state[pivot.c + index][pivot.l + 1];
            new_state[2][index] = this._state[pivot.c + index][pivot.l];
        }
        return new_state;
    }

    _rotate_anticlockwise(pivot) {
        let new_state = this._init_empty_board();

        for (let index = 0; index < 3; ++index) {
            new_state[0][2 - index] = this._state[pivot.c + index][pivot.l];
            new_state[1][2 - index] = this._state[pivot.c + index][pivot.l + 1];
            new_state[2][2 - index] = this._state[pivot.c + index][pivot.l + 2];
        }
        return new_state;
    }

    _set(marble_number, state, phase) {
        this._marble_number = marble_number;
        this._phase = phase;
        this._state = [];
        for (let c = 0; c < 6; ++c) {
            let column = [];

            for (let l = 0; l < 6; ++l) {
                column.push(state[c][l]);
            }
            this._state.push(column);
        }
    }
}

export default Engine;
