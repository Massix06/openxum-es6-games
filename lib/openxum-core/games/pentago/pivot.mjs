"use strict";

const Pivot = [
    {l: 0, c: 0},
    {l: 0, c: 3},
    {l: 3, c: 3},
    {l: 3, c: 0}
];

export default Pivot;
