"use strict";

import Color from './color.mjs';
import Coordinates from './coordinates.mjs';
import MarbleColor from './marble_color.mjs';
import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';

class Move extends OpenXum.Move {
    constructor(t, c, to, mc, f) {
        super();
        this._type = t;
        this._color = c;
        this._to = to;
        this._marble_color = mc;
        this._from = f;
    }

// public methods
    color() {
        return this._color;
    }

    coordinates() {
        return this._coordinates;
    }

    decode(str) {
        const type = str.substring(0, 2);

        if (type === 'Pm') {
            this._type = MoveType.PUT_MARBLE;
            this._to = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
            this._marble_color = str.charAt(5) === 'B' ? MarbleColor.BLACK :
                str.charAt(5) === 'W' ? MarbleColor.WHITE : MarbleColor.GREY;
        } else if (type === 'Rr') {
            this._type = MoveType.REMOVE_RING;
            this._to = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
        } else if (type === 'Ca') {
            this._type = MoveType.CAPTURE;
            this._from = new Coordinates(str.charAt(3), parseInt(str.charAt(4)));
            this._to = new Coordinates(str.charAt(5), parseInt(str.charAt(6)));
        }
        this._color = str.charAt(2) === '1' ? Color.ONE : Color.TWO;
    }

    encode() {
        let str;

        if (this._type === MoveType.PUT_MARBLE) {
            str = 'Pm' + (this._color === Color.ONE ? '1' : '2') +
                this._to.to_string() +
                (this._marble_color === MarbleColor.BLACK ? 'B' :
                    this._marble_color === MarbleColor.WHITE ? 'W' : 'G');
        } else if (this._type === MoveType.REMOVE_RING) {
            str = 'Rr' + (this._color === Color.ONE ? '1' : '2') + this._to.to_string();
        } else { // _type === MoveType.CAPTURE
            str = 'Ca' + (this._color === Color.ONE ? '1' : '2') + this._from.to_string() + this._to.to_string();
        }
        return str;
    }

    from() {
        return this._from;
    }

    from_object(data) {
        this._type = data.type;
        this._color = data.color;
        this._marble_color = data.marble_color;
        this._from = new Coordinates(data.from.letter, data.from.number);
        this._to = new Coordinates(data.to.letter, data.to.number);
    }

    marble_color() {
        return this._marble_color;
    }

    to() {
        return this._to;
    }

    to_object() {
        return {
            type: this._type,
            color: this._color,
            marble_color: this._marble_color,
            from: this._from === null ? {letter: -1, number: -1} : {
                letter: this._from._letter,
                number: this._from._number
            },
            to: this._to === null ? {letter: -1, number: -1} : {letter: this._to._letter, number: this._to._number}
        };
    }

    to_string() {
        let str = 'player ' + (this._color === Color.ONE ? '1' : '2') + ': ';

        if (this._type === MoveType.PUT_MARBLE) {
            str += 'put ' + (this._marble_color === MarbleColor.BLACK ? 'black' :
                    this._marble_color === MarbleColor.WHITE ? 'white' : 'grey') +
                ' marble to ' + this._to.to_string();
        } else if (this._type === MoveType.REMOVE_RING) {
            str += 'remove ring to ' + this._to.to_string();
        } else { // _type === MoveType.CAPTURE
            str += 'capture ' + (this._marble_color === MarbleColor.BLACK ? 'black' :
                this._marble_color === MarbleColor.WHITE ? 'white' : 'grey') + ' marble at ' + this._to.to_string() + ' with ' + this._from.to_string();
        }
        return str;
    }

    type() {
        return this._type;
    }
}

export default Move;
