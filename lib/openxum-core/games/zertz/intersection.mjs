"use strict";

import Color from './color.mjs';
import MarbleColor from './marble_color.mjs';
import State from './state.mjs';

class Intersection {
    constructor(c) {
        this._coordinates = c;
        this._state = State.VACANT;
    }

// public methods
    clone() {
        let intersection = new Intersection(this._coordinates.clone());

        intersection.set(this._state);
        return intersection;
    }

    color() {
        if (this._state === State.VACANT || this._state === State.EMPTY) {
            return MarbleColor.NONE;
        } else if (this._state === State.BLACK_MARBLE) {
            return MarbleColor.BLACK;
        } else if (this._state === State.GREY_MARBLE) {
            return MarbleColor.GREY;
        } else {
            return MarbleColor.WHITE;
        }
    }

    coordinates() {
        return this._coordinates;
    }

    hash() {
        return this._coordinates.hash();
    }

    letter() {
        return this._coordinates.letter();
    }

    marble_is_present() {
        return this._state === State.BLACK_MARBLE || this._state === State.WHITE_MARBLE || this._state === State.GREY_MARBLE;
    }

    number() {
        return this._coordinates.number();
    }

    put_marble(color) {
        if (color === MarbleColor.BLACK) {
            this._state = State.BLACK_MARBLE;
        } else if (color === MarbleColor.WHITE) {
            this._state = State.WHITE_MARBLE;
        } else if (color === MarbleColor.GREY) {
            this._state = State.GREY_MARBLE;
        }
    }

    remove_marble() {
        this._state = State.VACANT;
    }

    remove_ring() {
        this._state = State.EMPTY;
    }

    state() {
        return this._state;
    }

    set(state) {
        this._state = state;
    }

    to_string() {
        if (this._state === State.VACANT) {
            return '[V  ]';
        }
        if (this._state === State.EMPTY) {
            return '[E  ]';
        } else if (this._state === State.BLACK_MARBLE) {
            return '[BM ]';
        } else if (this._state === State.GREY_MARBLE) {
            return '[GM ]';
        } else {
            return '[WM ]';
        }
    }
}

export default Intersection;
