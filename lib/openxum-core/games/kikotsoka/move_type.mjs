"use strict";

const MoveType = {PUT_SHIDO: 0, PUT_PIECE: 1, CHOICE_PIECE: 2, CHOICE_PATTERN: 3, PASS: 4};

export default MoveType;