"use strict";

import Pentago from '../../../openxum-core/games/pentago/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);

        this._selected_coordinates = null;
        this._selected_board = null;
        this._selected_direction = null;

        this._pointerX = -1;
        this._pointerY = -1;
    }

// public methods
    draw() {
        this._context.lineWidth = 1;

        // background
        this._context.fillStyle = "#000000";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        this._draw_grid();
        this._draw_state();
        if (this._engine.phase() === Pentago.Phase.PUT_MARBLE) {
            this._show_intersection();
        } else if (this._engine.phase() === Pentago.Phase.ROTATE) {
            this._show_rotation_arrows();
        }
    }

    get_move() {
        if (this._engine.phase() === Pentago.Phase.PUT_MARBLE) {
            return new Pentago.Move(Pentago.MoveType.PUT_MARBLE, this._engine.current_color(), this._selected_coordinates);
        } else if (this._engine.phase() === Pentago.Phase.ROTATE) {
            return new Pentago.Move(Pentago.MoveType.ROTATE, this._engine.current_color(), this._selected_board, this._selected_direction);
        }
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._deltaX = (this._width * 0.95 - 40) / 6;
        this._deltaY = (this._height * 0.95 - 40) / 6;
        this._offsetX = this._width / 2 - this._deltaX * 3;
        this._offsetY = this._height / 2 - this._deltaY * 3;

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });
        this._canvas.addEventListener("mousemove", (e) => {
            this._on_move(e);
        });

        this.draw();
    }

    unselect() {
        this._selected_coordinates = null;
        this._selected_board = null;
        this._selected_direction = null;
    }

    // private methods
    _compute_coordinates(letter, number) {
        return [
            this._offsetX + ((letter - 'a'.charCodeAt(0)) + 0.5) * this._deltaX,
            this._offsetY + ((number - 1) + 0.5) * this._deltaY];
    }

    _compute_letter(x, y) {
        const index = Math.floor((x - this._offsetX) / this._deltaX);
        const x_ref = this._offsetX + this._deltaX * index;
        const x_ref2 = this._offsetX + this._deltaX * (index + 1);
        let letter = 'X';

        if (x >= x_ref && x <= x_ref2) {
            letter = String.fromCharCode('a'.charCodeAt(0) + index);
        }
        return letter;
    }

    _compute_number(x, y) {
        const index = Math.floor((y - this._offsetY) / this._deltaY);
        const y_ref = this._offsetY + this._deltaY * index;
        const y_ref2 = this._offsetY + this._deltaY * (index + 1);
        let number = -1;

        if (y >= y_ref && y <= y_ref2) {
            number = index + 1;
        }
        return number;
    }

    _compute_pointer(x, y) {
        let change = false;
        const letter = this._compute_letter(x, y);

        if (letter !== 'X') {
            const number = this._compute_number(x, y);

            if (number !== -1) {
                const pt = this._compute_coordinates(letter.charCodeAt(0), number);

                this._pointerX = pt[0];
                this._pointerY = pt[1];
                change = true;
            } else {
                if (this._pointerX !== -1) {
                    this._pointerX = this._pointerY = -1;
                    change = true;
                }
            }
        } else {
            if (this.__pointerX !== -1) {
                this.__pointerX = this.__pointerY = -1;
                change = true;
            }
        }
        return change;
    }

    _draw_rotation_arrow(x, y, orientation) {
        const width = 40;
        const height = 10;
        const arrowW = 0.35 * width;
        const arrowH = 0.75 * height;
        const points = [{x: 0, y: (height - arrowH) / 2},
            {x: (width - arrowW), y: (height - arrowH) / 2},
            {x: (width - arrowW), y: 0},
            {x: width, y: height / 2},
            {x: (width - arrowW), y: height},
            {x: (width - arrowW), y: height - ((height - arrowH) / 2)},
            {x: 0, y: height - ((height - arrowH) / 2)}];
        const s = Math.sin(orientation);
        const c = Math.cos(orientation);

        for (let i = 0; i < points.length; i++) {
            points[i].y -= height / 2;
        }
        for (let i = 0; i < points.length; i++) {
            const xnew = points[i].x * c - points[i].y * s;
            const ynew = points[i].x * s + points[i].y * c;

            points[i].x = xnew;
            points[i].y = ynew + height / 2;
        }
        this._context.fillStyle = "#FF0000";
        this._context.beginPath();
        this._context.moveTo(points[0].x + x, points[0].y + y);
        for (let i = 1; i < points.length; i++) {
            this._context.lineTo(points[i].x + x, points[i].y + y);
        }
        this._context.closePath();
        this._context.fill();
    }

    _draw_hole(x, y, width) {
        let gr = this._context.createRadialGradient(x, y, width / 5, x, y, width);

        this._context.beginPath();
        gr.addColorStop(1, '#000000');
        gr.addColorStop(0, '#ece88c');
        this._context.fillStyle = gr;
        this._context.arc(x, y, width / 2, 0.0, 2 * Math.PI, false);
        this._context.closePath();
        this._context.fill();
    }

    _draw_grid() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";

        // background
        this._context.fillStyle = 'white';
        this._context.beginPath();
        this._context.moveTo(this._offsetX - 10, this._offsetY - 10);
        this._context.lineTo(this._offsetX + 6 * this._deltaX + 10, this._offsetY - 10);
        this._context.lineTo(this._offsetX + 6 * this._deltaX + 10, this._offsetY + 6 * this._deltaY + 10);
        this._context.lineTo(this._offsetX - 10, this._offsetY + 6 * this._deltaY + 10);
        this._context.moveTo(this._offsetX - 10, this._offsetY - 10);
        this._context.closePath();
        this._context.fill();

        // grid
        for (let i = 0; i < 6; ++i) {
            for (let j = 0; j < 6; ++j) {
                this._context.fillStyle = '#ece88c';
                this._context.lineWidth = 1;
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX - 2, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY - 2);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                this._context.fill();
                this._context.stroke();
                this._draw_hole(this._offsetX + (i + 0.5) * this._deltaX, this._offsetY + (j + 0.5) * this._deltaY, this._deltaX / 2);
            }
        }
    }

    _draw_state() {
        for (let i = 0; i < 6; ++i) {
            for (let j = 0; j < 6; ++j) {
                const state = this._engine.get_state(String.fromCharCode('a'.charCodeAt(0) + i) + (j + 1));

                if (state !== Pentago.State.EMPTY) {
                    Graphics.marble.draw_marble(this._context, this._offsetX + (i + 0.5) * this._deltaX,
                        this._offsetY + (j + 0.5) * this._deltaY, this._deltaX / 1.5, state === Pentago.Color.BLACK ? 'black' : 'white');
                }
            }
        }
    }

    _get_click_position(e) {
        let rect = this._canvas.getBoundingClientRect();

        return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);

            if (this._engine.phase() === Pentago.Phase.PUT_MARBLE) {
                const letter = this._compute_letter(pos.x, pos.y);

                if (letter !== 'X') {
                    const number = this._compute_number(pos.x, pos.y);

                    if (number !== -1) {
                        this._selected_coordinates = new Pentago.Coordinates(letter, number);
                        this._manager.play();
                    }
                }
            } else if (this._engine.phase() === Pentago.Phase.ROTATE) {
                let ok = false;

                if (pos.x > 0 && pos.x <= 25) {
                    if (pos.y > 20 && pos.y <= 20 + 40) {
                        this._selected_board = Pentago.Board.TOP_LEFT;
                        this._selected_direction = Pentago.Direction.ANTI_CLOCKWISE;
                        ok = true;
                    } else if (pos.y < this._height - 20 && pos.y >= this._height - 20 - 40) {
                        this._selected_board = Pentago.Board.BOTTOM_LEFT;
                        this._selected_direction = Pentago.Direction.CLOCKWISE;
                        ok = true;
                    }
                } else if (pos.x > 25 && pos.x <= 40 + 25) {
                    if (pos.y > 0 && pos.y <= 20) {
                        this._selected_board = Pentago.Board.TOP_LEFT;
                        this._selected_direction = Pentago.Direction.CLOCKWISE;
                        ok = true;
                    } else if (pos.y < this._height && pos.y >= this._height - 20) {
                        this._selected_board = Pentago.Board.BOTTOM_LEFT;
                        this._selected_direction = Pentago.Direction.ANTI_CLOCKWISE;
                        ok = true;
                    }
                } else if (pos.x >= this._width - 25 - 40 && pos.x < this._width - 25) {
                    if (pos.y > 0 && pos.y <= 20) {
                        this._selected_board = Pentago.Board.TOP_RIGHT;
                        this._selected_direction = Pentago.Direction.ANTI_CLOCKWISE;
                        ok = true;
                    } else if (pos.y < this._height && pos.y >= this._height - 20) {
                        this._selected_board = Pentago.Board.BOTTOM_RIGHT;
                        this._selected_direction = Pentago.Direction.CLOCKWISE;
                        ok = true;
                    }
                } else if (pos.x >= this._width - 25 && pos.x < this._width) {
                    if (pos.y > 20 && pos.y <= 20 + 40) {
                        this._selected_board = Pentago.Board.TOP_RIGHT;
                        this._selected_direction = Pentago.Direction.CLOCKWISE;
                        ok = true;
                    } else if (pos.y < this._height - 20 && pos.y >= this._height - 20 - 40) {
                        this._selected_board = Pentago.Board.BOTTOM_RIGHT;
                        this._selected_direction = Pentago.Direction.ANTI_CLOCKWISE;
                        ok = true;
                    }
                }
                if (ok) {
                    this._manager.play();
                }
            }
        }
    }

    _on_move(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);
            const letter = this._compute_letter(pos.x, pos.y);

            if (letter !== 'X') {
                const number = this._compute_number(pos.x, pos.y);

                if (number !== -1) {
                    if (this._compute_pointer(pos.x, pos.y)) {
                        this._manager.redraw();
                    }
                }
            }
        }
    }

    _show_intersection() {
        if (this._pointerX !== -1 && this._pointerY !== -1) {
            this._context.fillStyle = "#0000ff";
            this._context.strokeStyle = "#0000ff";
            this._context.lineWidth = 1;
            this._context.beginPath();
            this._context.arc(this._pointerX, this._pointerY, 5, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.fill();
            this._context.stroke();
        }
    }

    _show_rotation_arrows() {
        // TOP LEFT
        this._draw_rotation_arrow(25, 8, 0);
        this._draw_rotation_arrow(12, 20, Math.PI / 2);
        // TOP RIGHT
        this._draw_rotation_arrow(this._width - 25, 8, Math.PI);
        this._draw_rotation_arrow(this._width - 12, 20, Math.PI / 2);
        //  BOTTOM LEFT
        this._draw_rotation_arrow(25, this._height - 18, 0);
        this._draw_rotation_arrow(12, this._height - 28, 3 * Math.PI / 2);
        // BOTTOM RIGHT
        this._draw_rotation_arrow(this._width - 25, this._height - 18, Math.PI);
        this._draw_rotation_arrow(this._width - 12, this._height - 28, 3 * Math.PI / 2);
    }
}

export default Gui;
