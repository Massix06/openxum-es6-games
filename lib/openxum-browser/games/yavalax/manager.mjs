"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new Yavalax.Move();
    }

    get_current_color() {
        return this.engine().current_color() === Yavalax.Color.BLUE ? 'White' : 'bleu';
    }

    static get_name() {
        return 'yavalax';
    }

    get_winner_color() {
        return this.engine().winner_is() === Yavalax.Color.WHITE ? 'White' : 'Bleu';
    }

    process_move() {
    }
}

export default Manager;
