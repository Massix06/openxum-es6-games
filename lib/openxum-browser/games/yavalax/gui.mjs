"use strict";

import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._selected_cell = null;
        this._selected_color = Yavalax.Color.NONE;
    }

    draw() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#6BA3FE";
        Graphics.board.draw_round_rect(this._context, 40, 41, this._canvas.width-75, this._canvas.height-72, 5, true, true);

        this._draw_state();
        this._draw_grid();

    }

    get_move() {
        return new Yavalax.Move(Yavalax.MoveType.PUT_PIECE, this._engine.current_color(), new Yavalax.Coordinates(this._selected_cell.get_line(), this._selected_cell.get_column()));
    }

    move(move,color){
        this._manager.play();
    }


    unselect() {
        this._selected_piece = undefined;
        this.draw();
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._deltaX = (this._canvas.width * 0.94 - 40) / 13;
        this._deltaY = (this._canvas.height * 0.94 - 40) / 13;
        this._offsetX = this._deltaX;
        this._offsetY = this._deltaY;

        this._scaleX = this._canvas.height / this._canvas.offsetHeight;
        this._scaleY = this._canvas.width / this._canvas.offsetWidth;

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });

        this.draw();
    }


    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    _compute_coordinates(x, y) {
        return new Yavalax.Coordinates(Math.floor((x - this._offsetX) / this._deltaX), Math.floor((y - this._offsetY) / this._deltaY));
    }

    _draw_grid() {
        this._context.strokeStyle = "#4682B4";
        this._context.lineWidth = 2;


        for (let i = 0; i < 14; i++) {
            let x = this._offsetX + i * this._deltaX;
            this._context.beginPath();
            this._context.moveTo(x, this._offsetY);
            this._context.lineTo(x, this._offsetY + 13 * this._deltaY);
            this._context.closePath();
            this._context.stroke();
        }


        for (let j = 0; j < 14; j++) {
            let y = this._offsetY + j * this._deltaY;
            this._context.beginPath();
            this._context.moveTo(this._offsetX, y);
            this._context.lineTo(this._offsetX + 13 * this._deltaX, y);
            this._context.closePath();
            this._context.stroke();

        }}


    _draw_state() {
        for (let x = 0; x < 13; x++) {
            for (let y = 0; y < 13; y++) {
                if (this._engine._gameBoard[x][y] !== Yavalax.Color.NONE) {
                    this._draw_stone(x, y, this._engine._gameBoard[x][y]);
                }
            }
        }
    }

    _draw_stone(x, y, piece) {
        let radius = this._deltaX / 2.5;

        if (piece === Yavalax.Color.BLUE) {
            this._context.strokeStyle = "#000000";
            this._context.fillStyle = "#0000FF";
        } else if (piece === Yavalax.Color.WHITE) {
            this._context.strokeStyle = "#000000";
            this._context.fillStyle = "#ffffff";
        }
        this._context.lineWidth = 1;

        this._context.beginPath();
        this._context.arc(x * this._deltaX + this._offsetX + (this._deltaX / 2) - 1, y * this._deltaY + this._offsetY + (this._deltaY / 2) - 1, radius, 0.0, 2 * Math.PI);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();
    }

    _get_click_position(e) {
        const rect = this._canvas.getBoundingClientRect();
        return { x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY };
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);
            this._selected_cell = this._compute_coordinates(pos.x, pos.y);
            this._manager.play();
        }
    }
}

export default Gui;