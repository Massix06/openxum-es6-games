"use strict";

import Yavalax from '../../../openxum-core/games/yavalax/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Yavalax.Color.BLUE,
            init: Yavalax.Color.BLUE,
            list: [
                {key: Yavalax.Color.BLUE, value: 'bleu'},
                {key: Yavalax.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Yavalax.GameType.STANDARD,
            list: [
                {key: Yavalax.GameType.STANDARD, value: 'standard'}
            ]
        },
        opponent_color(color) {
            return color === Yavalax.Color.BLUE ? Yavalax.Color.WHITE : Yavalax.Color.BLUE;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
