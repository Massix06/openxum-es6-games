"use strict";

import Graphics from '../../graphics/index.mjs';
import Kikotsoka from '../../../openxum-core/games/kikotsoka/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._selected_cell = null;
        this._choices = [];
        this._selected_index = -1;
    }

// public methods
    draw() {
        // fond
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);

        this._context.lineWidth = 10;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#8B4513";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        this._draw_grid();
        this._draw_patterns();
        this._draw_state();
        if (this._engine.phase() === Kikotsoka.Phase.PUT_SHIDO || this._engine.phase() === Kikotsoka.Phase.PUT_PIECE) {
            this._draw_pass_button();
            this._draw_possible_moves();
        }
        if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PIECE) {
            this._draw_shido_piece_button();
        }
        if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PATTERN) {
            this._choices = this._engine._check_patterns();
            this._draw_choices();
        }
    }

    get_move() {
        if (this._engine.phase() === Kikotsoka.Phase.PUT_SHIDO && this._selected_cell !== null) {
            return new Kikotsoka.Move(Kikotsoka.MoveType.PUT_SHIDO, this._engine._color,
                new Kikotsoka.Coordinates(this._selected_cell.column, this._selected_cell.line), -1);
        } else if (this._engine.phase() === Kikotsoka.Phase.PUT_PIECE && this._selected_cell !== null) {
            return new Kikotsoka.Move(Kikotsoka.MoveType.PUT_PIECE, this._engine._color,
                new Kikotsoka.Coordinates(this._selected_cell.column, this._selected_cell.line), -1);
        } else if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PATTERN && this._selected_index !== -1) {
            return new Kikotsoka.Move(Kikotsoka.MoveType.CHOICE_PATTERN, this._engine._color, null, this._selected_index);
        } else if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PIECE && this._selected_index !== -1) {
            return new Kikotsoka.Move(Kikotsoka.MoveType.CHOICE_PIECE, this._engine._color, null, this._selected_index);
        } else {
            return new Kikotsoka.Move(Kikotsoka.MoveType.PASS, this._engine._color, null, -1);
        }
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._height = this._canvas.height;
        this._width = this._canvas.width;
        this._deltaX = (this._width * 0.95 - 50) / this._engine._size;
        this._deltaY = (this._height * 0.95 - 30) / this._engine._size;
        this._offsetX = this._width / 2 - this._deltaX * this._engine._size / 2;
        this._offsetY = this._height / 2 - this._deltaY * this._engine._size / 2;

        this._scaleX = this._height / this._canvas.offsetHeight;
        this._scaleY = this._width / this._canvas.offsetWidth;

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });

        this.draw();
    }

    unselect() {
        this._selected_cell = null;
    }

    // private methods
    _compute_coordinates(x, y) {
        return {
            column: Math.floor((x - this._offsetX) / this._deltaX),
            line: Math.floor((y - this._offsetY) / this._deltaY)
        };
    }

    _draw_choices() {
        this._choices.forEach((origin) => {
            const x = this._offsetX + origin.c * this._deltaX;
            const y = this._offsetY + origin.l * this._deltaY;
            const radius = 8;
            const width = this._deltaX * 3;
            const height = this._deltaY * 3;

            this._context.lineWidth = 1;
            this._context.strokeStyle = "#ffff00";
            this._context.fillStyle = "#ffff00";
            this._context.beginPath();
            this._context.arc(x + this._deltaX / 2, y + this._deltaY / 2, this._deltaX * (1.0 / 3 + 1.0 / 10) / 3, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.stroke();
            this._context.fill();

            this._context.lineWidth = 4;
            this._context.beginPath();
            this._context.moveTo(x + radius, y);
            this._context.lineTo(x + width - radius, y);
            this._context.quadraticCurveTo(x + width, y, x + width, y + radius);
            this._context.lineTo(x + width, y + height - radius);
            this._context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
            this._context.lineTo(x + radius, y + height);
            this._context.quadraticCurveTo(x, y + height, x, y + height - radius);
            this._context.lineTo(x, y + radius);
            this._context.quadraticCurveTo(x, y, x + radius, y);
            this._context.closePath();
            this._context.stroke();
        });
    }

    _draw_grid() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        for (let i = 0; i < this._engine._size; ++i) {
            for (let j = 0; j < this._engine._size; ++j) {
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX, this._offsetY + (j + 1) * this._deltaY);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                this._context.stroke();
            }
        }
    }

    _draw_button(pos_x, pos_y, text_width, text) {
        const text_height = 23;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#757D75";

        this._context.beginPath();
        this._context.moveTo(this._offsetX + pos_x, this._offsetY + pos_y);
        this._context.lineTo(this._offsetX + pos_x + text_width, this._offsetY + pos_y);
        this._context.lineTo(this._offsetX + pos_x + text_width, this._offsetY + pos_y + text_height);
        this._context.lineTo(this._offsetX + pos_x, this._offsetY + pos_y + text_height);
        this._context.lineTo(this._offsetX + pos_x, this._offsetY + pos_y);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();

        this._context.fillStyle = "#ffffff";
        this._context.font = "20px Colibri";
        this._context.textAlign = "center";
        this._context.textBaseline = "top";
        this._context.beginPath();
        this._context.fillText(text, this._offsetX + pos_x + text_width / 2, this._offsetY + pos_y);
        this._context.closePath();
        this._context.fill();
    }

    _draw_pass_button() {
        const text_width = 100;
        const pos_x = (this._width - 2 * this._offsetX - text_width) / 2;
        const pos_y = this._height - 2 * this._offsetY + 3;

        this._draw_button(pos_x, pos_y, text_width, "Pass");
    }

    _draw_shido_piece_button() {
        const text_width = 100;
        const pos_y = this._height - 2 * this._offsetY + 3;

        this._draw_button((this._width - 2 * this._offsetX) / 4 - text_width / 2, pos_y, 100, "Shido");
        this._draw_button(3 * (this._width - 2 * this._offsetX) / 4 - text_width / 2, pos_y, 100, "Piece");
    }

    _draw_pattern(x, y, color, pattern) {
        const width = 10;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#ffffff";
        for (let l = 0; l < 3; ++l) {
            for (let c = 0; c < 3; ++c) {
                if (pattern[l][c] === 1) {
                    if (color === Kikotsoka.Color.BLACK) {
                        this._context.strokeStyle = "#ffffff";
                        this._context.fillStyle = "#000000";
                    } else {
                        this._context.strokeStyle = "#000000";
                        this._context.fillStyle = "#ffffff";
                    }
                    this._context.beginPath();
                    this._context.moveTo(x + c * width, y + l * width);
                    this._context.lineTo(x + (c + 1) * width, y + l * width);
                    this._context.lineTo(x + (c + 1) * width, y + (l + 1) * width);
                    this._context.lineTo(x + c * width, y + (l + 1) * width);
                    this._context.lineTo(x + c * width, y + l * width);
                    this._context.closePath();
                    this._context.fill();
                    this._context.stroke();
                }
            }
        }
    }

    _draw_patterns_with_color(color) {
        const current_level = color === Kikotsoka.Color.BLACK ? this._engine._black_level : this._engine._white_level;

        if (current_level < 5) {
            const patterns = Kikotsoka.Patterns[current_level];

            for (let i = 0; i < patterns.length; ++i) {
                this._draw_pattern(color === Kikotsoka.Color.BLACK ? 4 : this._width - 36, 32 + 50 * i, color, patterns[i]);
            }
        }
    }

    _draw_patterns() {
        this._draw_patterns_with_color(Kikotsoka.Color.BLACK);
        this._draw_patterns_with_color(Kikotsoka.Color.WHITE);
    }

    _draw_piece_number() {
        this._context.fillStyle = "#ffffff";
        this._context.textAlign = "start";
        this._context.textBaseline = "middle";
        this._context.font = 0.5 * this._offsetY + "px calibri";
        this._context.fillText("captured pieces: black: " + this._engine._black_captured_piece_number +
            "  white: " + this._engine._white_captured_piece_number, this._offsetX, this._offsetY / 2);
        this._context.textAlign = "end";
        this._context.fillText("remaining pieces: black: " + this._engine._black_piece_number +
            "  white: " + this._engine._white_piece_number, this._width - this._offsetX, this._offsetY / 2);
    }

    _draw_possible_moves() {
        const list = this._engine.get_possible_move_list();

        if (list[0]._type !== Kikotsoka.Phase.PASS && list[0]._type !== Kikotsoka.Phase.CHOICE_PATTERN) {
            for (let index = 0; index < list.length; ++index) {
                const move = list[index];
                const coordinates = move.to();
                const x = this._offsetX + coordinates.column() * this._deltaX + this._deltaX / 2;
                const y = this._offsetY + coordinates.line() * this._deltaY + this._deltaY / 2;

                this._context.strokeStyle = "#00ff00";
                this._context.fillStyle = "#00ff00";
                this._context.lineWidth = 1;
                this._context.beginPath();
                this._context.arc(x, y, this._deltaX * (1.0 / 3 + 1.0 / 10) / 3, 0.0, 2 * Math.PI);
                this._context.closePath();
                this._context.stroke();
                this._context.fill();
            }
        }
    }

    _draw_state() {
        this._draw_piece_number();
        for (let line = 0; line < this._engine._size; ++line) {
            for (let column = 0; column < this._engine._size; ++column) {
                const state = this._engine._board[line][column];
                const x = this._offsetX + column * this._deltaX + this._deltaX / 2;
                const y = this._offsetY + line * this._deltaY + this._deltaY / 2;

                if (state !== Kikotsoka.State.VACANT && state !== Kikotsoka.State.BLOCKED_IN_BLACK && state !== Kikotsoka.State.BLOCKED_IN_WHITE) {
                    if (state === Kikotsoka.State.BLACK_SHIDO || state === Kikotsoka.State.WHITE_SHIDO ||
                        state === Kikotsoka.State.BLACK_SHIDO_BLOCKED || state === Kikotsoka.State.WHITE_SHIDO_BLOCKED) {
                        const color = state === Kikotsoka.State.BLACK_SHIDO || state === Kikotsoka.State.BLACK_SHIDO_BLOCKED ? "black" : "white";

                        Graphics.ring.draw_ring(this._context, x, y, 2 * this._deltaX / 3, color, "#8B4513");
                    } else {
                        const color = state === Kikotsoka.State.BLACK || state === Kikotsoka.State.BLACK_BLOCKED ? "black" : "white";

                        Graphics.marble.draw_marble(this._context, x, y, 2 * this._deltaX / 3, color);
                    }
                }
                if (state === Kikotsoka.State.BLACK_BLOCKED || state === Kikotsoka.State.WHITE_BLOCKED ||
                    state === Kikotsoka.State.BLACK_SHIDO_BLOCKED || state === Kikotsoka.State.WHITE_SHIDO_BLOCKED) {
                    this._context.strokeStyle = "#ffff00";
                    this._context.lineWidth = 4;
                    this._context.beginPath();
                    this._context.arc(x, y, this._deltaX * (1.0 / 3 + 1.0 / 10), 0.0, 2 * Math.PI);
                    this._context.closePath();
                    this._context.stroke();
                }
                if (state === Kikotsoka.State.BLOCKED_IN_BLACK || state === Kikotsoka.State.BLOCKED_IN_WHITE) {
                    this._context.strokeStyle = "#000000";
                    this._context.fillStyle = "#ff0000";
                    this._context.lineWidth = 1;
                    this._context.beginPath();
                    this._context.arc(x, y, this._deltaX * (1.0 / 10), 0.0, 2 * Math.PI);
                    this._context.closePath();
                    this._context.stroke();
                    this._context.fill();
                }
            }
        }
    }

    _get_click_position(e) {
        const rect = this._canvas.getBoundingClientRect();

        return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);
            const text_width = 100;
            const pos_y = this._height - this._offsetY;

            if (pos.y > pos_y) {
                if (this._engine.phase() === Kikotsoka.Phase.PUT_PIECE) {
                    const pos_x = (this._width - text_width) / 2;

                    if (pos.x > pos_x && pos.x < pos_x + text_width) {
                        // todo: test coordinates pass button!
                        this._manager.play();
                    }
                } else if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PIECE) {
                    const pos_x = (this._width - 2 * this._offsetX) / 4;

                    this._selected_index = -1;
                    if (pos.x > pos_x - text_width / 2 + this._offsetX && pos.x < pos_x + text_width / 2 + this._offsetX) {
                        this._selected_index = 0;
                    } else if (pos.x > 3 * pos_x - text_width / 2 + this._offsetX && pos.x < 3 * pos_x + text_width / 2 + this._offsetX) {
                        this._selected_index = 1;
                    }
                    this._manager.play();
                }
            } else {
                if (this._engine.phase() === Kikotsoka.Phase.PUT_SHIDO || this._engine.phase() === Kikotsoka.Phase.PUT_PIECE) {
                    this._selected_cell = this._compute_coordinates(pos.x, pos.y);
                    if (this._engine.is_valid(this._selected_cell.line, this._selected_cell.column)) {
                        this._manager.play();
                    } else {
                        this._selected_cell = null;
                    }
                } else if (this._engine.phase() === Kikotsoka.Phase.CHOICE_PATTERN) {
                    const select = this._compute_coordinates(pos.x, pos.y);
                    let found = false;
                    let i = 0;

                    while (!found && i < this._choices.length) {
                        found = select.column === this._choices[i].c && select.line === this._choices[i].l;
                        if (!found) {
                            ++i;
                        }
                    }
                    if (found) {
                        this._selected_index = i;
                        this._manager.play();
                    }
                }
            }
        }
    }
}

export default Gui;
