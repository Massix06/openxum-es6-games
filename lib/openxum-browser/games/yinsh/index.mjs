"use strict";

import Yinsh from '../../../openxum-core/games/yinsh/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
    Gui: Gui,
    Manager: Manager,
    Settings: {
        ai: {
            mcts: AI.RandomPlayer // TODO: MCTSPlayer
        },
        colors: {
            first: Yinsh.Color.BLACK,
            init: Yinsh.Color.BLACK,
            list: [
                {key: Yinsh.Color.BLACK, value: 'black'},
                {key: Yinsh.Color.WHITE, value: 'white'}
            ]
        },
        modes: {
            init: Yinsh.GameType.REGULAR,
            list: [
                {key: Yinsh.GameType.BLITZ, value: 'blitz'},
                {key: Yinsh.GameType.REGULAR, value: 'regular'}
            ]
        },
        opponent_color(color) {
            return color === Yinsh.Color.BLACK ? Yinsh.Color.WHITE : Yinsh.Color.BLACK;
        },
        types: {
            init: 'ai',
            list: [
                {key: 'gui', value: 'GUI'},
                {key: 'ai', value: 'AI'},
                {key: 'online', value: 'Online'},
                {key: 'offline', value: 'Offline'}
            ]
        }
    }
};
