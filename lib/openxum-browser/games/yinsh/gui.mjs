"use strict";

import Yinsh from '../../../openxum-core/games/yinsh/index.mjs';
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

// grid constants definition
const begin_letter = ['B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'];
const end_letter = ['E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'];
const begin_number = [2, 1, 1, 1, 1, 2, 2, 3, 4, 5, 7];
const end_number = [5, 7, 8, 9, 10, 10, 11, 11, 11, 11, 10];
const begin_diagonal_letter = ['B', 'A', 'A', 'A', 'A', 'B', 'B', 'C', 'D', 'E', 'G'];
const end_diagonal_letter = ['E', 'G', 'H', 'I', 'J', 'J', 'K', 'K', 'K', 'K', 'J'];
const begin_diagonal_number = [7, 5, 4, 3, 2, 2, 1, 1, 1, 1, 2];
const end_diagonal_number = [10, 11, 11, 11, 11, 10, 10, 9, 8, 7, 5];

// enums definition
const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);

        this._tolerance = 15;
        this._delta_x = 0;
        this._delta_y = 0;
        this._delta_xy = 0;
        this._offset = 0;
        this._pointerX = -1;
        this._pointerY = -1;
        this._selected_coordinates = new Yinsh.Coordinates('X', -1);
        this._selected_ring = new Yinsh.Coordinates('X', -1);
        this._selected_row = [];
        this._row_index = -1;
    }

// public methods
    draw() {
        this._compute_deltas();
        this._context.lineWidth = 1;

        // background
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#ffffff";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        // grid
        this._draw_grid();
        this._draw_coordinates();

        // state
        this._draw_state();

        if (this._engine.phase() === Yinsh.Phase.MOVE_RING && this._selected_ring.is_valid()) {
            this._draw_possible_moving();
        } else if (this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_AFTER || this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_BEFORE) {
            this._draw_remove_markers();
        }

        //intersection
        this._show_intersection();
    }

    get_move() {
        let move = null;

        if (this._engine.phase() === Yinsh.Phase.PUT_RING) {
            move = new Yinsh.Move(Yinsh.MoveType.PUT_RING, this._engine.current_color(), this._selected_coordinates);
        } else if (this._engine.phase() === Yinsh.Phase.PUT_MARKER) {
            move = new Yinsh.Move(Yinsh.MoveType.PUT_MARKER, this._engine.current_color(), this._selected_coordinates);
        } else if (this._engine.phase() === Yinsh.Phase.MOVE_RING) {
            move = new Yinsh.Move(Yinsh.MoveType.MOVE_RING, this._engine.current_color(), this._selected_ring, this._selected_coordinates);
        } else if (this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_AFTER ||
            this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_BEFORE) {
            move = new Yinsh.Move(Yinsh.MoveType.REMOVE_ROW, this._engine.current_color(), this._selected_row);
        } else if (this._engine.phase() === Yinsh.Phase.REMOVE_RING_AFTER ||
            this._engine.phase() === Yinsh.Phase.REMOVE_RING_BEFORE) {
            move = new Yinsh.Move(Yinsh.MoveType.REMOVE_RING, this._engine.current_color(), this._selected_coordinates);
        }
        return move;
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });
        this._canvas.addEventListener("mousemove", (e) => {
            this._on_move(e);
        });

        this.draw();
    }

    unselect() {
        this._selected_coordinates = new Yinsh.Coordinates('X', -1);
        this._selected_ring = new Yinsh.Coordinates('X', -1);
        this._selected_row = [];
        this._row_index = -1;
    }

// private methods
    _compute_coordinates(letter, number) {
        const index_x = letter - 'A'.charCodeAt(0);
        const x = this._offset + this._delta_x * index_x;
        const y = this._offset + 7 * this._delta_y + this._delta_xy * index_x - (number - 1) * this._delta_y;

        return [x, y];
    }

    _compute_deltas() {
        this._offset = 30;
        this._delta_x = (this._width - 2 * this._offset) / 10.0;
        this._delta_y = this._delta_x;
        this._delta_xy = this._delta_y / 2;
    }

    _compute_letter(x, y) {
        const index = Math.floor((x - this._offset) / this._delta_x);
        const x_ref = this._offset + this._delta_x * index;
        const x_ref_2 = this._offset + this._delta_x * (index + 1);
        let _letter = 'X';

        if (x < this._offset) {
            _letter = 'A';
        } else if (x <= x_ref + this._delta_x / 2 && x >= x_ref && x <= x_ref + this._tolerance) {
            _letter = letters[index];
        } else if (x > x_ref + this._delta_x / 2 && x >= x_ref_2 - this._tolerance) {
            _letter = letters[index + 1];
        }
        return _letter;
    }

    _compute_number(x, y) {
        const pt = this._compute_coordinates('A'.charCodeAt(0), 1);

        // translation to A1 and rotation
        const X = x - pt[0];
        const Y = y - pt[1];
        const sin_alpha = 1.0 / Math.sqrt(5);
        const cos_alpha = 2.0 * sin_alpha;

        const x2 = Math.floor((X * sin_alpha - Y * cos_alpha) + pt[0]);
        const delta_x2 = Math.floor(this._delta_x * cos_alpha);

        const index = Math.floor((x2 - this._offset) / delta_x2);
        const x_ref = Math.floor(this._offset + delta_x2 * index);
        const x_ref_2 = Math.floor(this._offset + delta_x2 * (index + 1));

        let _number = -1;

        if (x2 > 0 && x2 < this._offset) {
            _number = 1;
        } else if (x2 <= x_ref + delta_x2 / 2 && x2 >= x_ref && x2 <= x_ref + this._tolerance) {
            _number = index + 1;
        } else if (x2 > x_ref + delta_x2 / 2 && x2 >= x_ref_2 - this._tolerance) {
            _number = index + 2;
        }
        return _number;
    }

    _compute_pointer(x, y) {
        let change = false;
        let letter = this._compute_letter(x, y);

        if (letter !== 'X') {
            let number = this._compute_number(x, y);

            if (number !== -1) {
                if (this._engine.exist_intersection(letter, number)) {
                    let pt = this._compute_coordinates(letter.charCodeAt(0), number);

                    this._pointerX = pt[0];
                    this._pointerY = pt[1];
                    change = true;
                } else {
                    this._pointerX = this._pointerY = -1;
                    change = true;
                }
            } else {
                if (this._pointerX !== -1) {
                    this._pointerX = this._pointerY = -1;
                    change = true;
                }
            }
        } else {
            if (this._pointerX !== -1) {
                this._pointerX = this._pointerY = -1;
                change = true;
            }
        }
        return change;
    }

    _draw_coordinates() {
        let pt;

        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#000000";
        this._context.font = "16px _sans";
        this._context.textBaseline = "top";
        // letters
        for (let l = 'A'.charCodeAt(0); l < 'L'.charCodeAt(0); ++l) {
            pt = this._compute_coordinates(l, begin_number[l - 'A'.charCodeAt(0)]);
            pt[0] -= 5;
            pt[1] += 20;
            this._context.fillText(String.fromCharCode(l), pt[0], pt[1]);
        }

        // numbers
        this._context.textBaseline = "bottom";
        for (let n = 1; n < 12; ++n) {
            pt = this._compute_coordinates(begin_letter[n - 1].charCodeAt(0), n);
            pt[0] -= 15 + (n > 9 ? 5 : 0);
            pt[1] -= 3;
            this._context.fillText(n.toString(), pt[0], pt[1]);
        }
    }

    _draw_grid() {
        let _pt_begin;
        let _pt_end;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#ffffff";

        for (let l = 'A'.charCodeAt(0); l < 'L'.charCodeAt(0); ++l) {
            let index = l - 'A'.charCodeAt(0);

            _pt_begin = this._compute_coordinates(l, begin_number[index]);
            _pt_end = this._compute_coordinates(l, end_number[index]);
            this._context.moveTo(_pt_begin[0], _pt_begin[1]);
            this._context.lineTo(_pt_end[0], _pt_end[1]);
        }

        for (let n = 1; n < 12; ++n) {
            _pt_begin = this._compute_coordinates(begin_letter[n - 1].charCodeAt(0), n);
            _pt_end = this._compute_coordinates(end_letter[n - 1].charCodeAt(0), n);
            this._context.moveTo(_pt_begin[0], _pt_begin[1]);
            this._context.lineTo(_pt_end[0], _pt_end[1]);
        }

        for (let i = 0; i < 11; ++i) {
            _pt_begin = this._compute_coordinates(begin_diagonal_letter[i].charCodeAt(0),
                begin_diagonal_number[i]);
            _pt_end = this._compute_coordinates(end_diagonal_letter[i].charCodeAt(0),
                end_diagonal_number[i]);
            this._context.moveTo(_pt_begin[0], _pt_begin[1]);
            this._context.lineTo(_pt_end[0], _pt_end[1]);
        }
        this._context.stroke();
    }

    _draw_marker(x, y, color) {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        if (color === Yinsh.Color.BLACK) {
            this._context.fillStyle = "#000000";
        } else if (color === Yinsh.Color.WHITE) {
            this._context.fillStyle = "rgb(192, 192, 192)";
        } else {
            this._context.fillStyle = "#ff0000";
        }
        this._context.beginPath();
        this._context.arc(x, y, this._delta_x * (1.0 / 3 - 1.0 / 10) - 1, 0.0, 2 * Math.PI, false);
        this._context.fill();
        this._context.stroke();
        this._context.closePath();
    }

    _draw_possible_moving() {
        const list = this._engine.get_possible_moving_list(this._selected_ring, this._engine.current_color(), true);

        this._context.strokeStyle = "#FFFFFF";
        this._context.fillStyle = "#FF0000";
        this._context.lineWidth = 1;
        for (let i = 0; i < list.length; ++i) {
            const pt = this._compute_coordinates(list[i].letter().charCodeAt(0), list[i].number());

            this._context.beginPath();
            this._context.arc(pt[0], pt[1], 10, 0.0, 2 * Math.PI, false);
            this._context.fill();
            this._context.stroke();
            this._context.closePath();
        }
    }

    _draw_remove_markers() {
        if (this._selected_row.length > 0) {
            for (let index = 0; index < this._selected_row.length; ++index) {
                const pt = this._compute_coordinates(this._selected_row[index].letter().charCodeAt(0), this._selected_row[index].number());

                this._draw_marker(pt[0], pt[1]);
            }
        }
    }

    _draw_ring(x, y, color) {
        this._context.beginPath();
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        this._context.arc(x, y, this._delta_x * (1.0 / 3 + 1.0 / 10), 0.0, 2 * Math.PI, false);
        this._context.stroke();
        this._context.arc(x, y, this._delta_x * (1.0 / 3 - 1.0 / 10) - 1, 0.0, 2 * Math.PI, false);
        this._context.stroke();
        this._context.closePath();

        this._context.beginPath();
        this._context.lineWidth = this._delta_x / 5;
        if (color === Yinsh.Color.BLACK) {
            this._context.strokeStyle = "#000000";
        } else {
            this._context.strokeStyle = "#ffffff";
        }
        this._context.arc(x, y, this._delta_x / 3, 0.0, 2 * Math.PI, false);
        this._context.stroke();
        this._context.closePath();
    }

    _draw_rows() {
        if (this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_AFTER ||
            this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_BEFORE) {
            const srows = [this._engine.get_rows(this._engine.current_color())];

            for (let i = 0; i < srows.length; ++i) {
                for (let j = 0; j < srows[i].length; ++j) {
                    const begin = srows[i][j][0];
                    const end = srows[i][j][srows[i][j].length - 1];
                    let pt1, pt2;
                    let alpha_1, beta_1;
                    let alpha_2, beta_2;

                    pt1 = this._compute_coordinates(begin.letter().charCodeAt(0), begin.number());
                    pt2 = this._compute_coordinates(end.letter().charCodeAt(0), end.number());

                    if (pt1[0] === pt2[0]) {
                        if (pt1[1] < pt2[1]) {
                            alpha_1 = Math.PI;
                            beta_1 = 0;
                            alpha_2 = 0;
                            beta_2 = Math.PI;
                        } else {
                            alpha_1 = 0;
                            beta_1 = Math.PI;
                            alpha_2 = Math.PI;
                            beta_2 = 0;
                        }
                    } else {
                        var omega_1 = Math.acos(1.0 / Math.sqrt(5));

                        if (pt1[0] < pt2[0]) {
                            if (pt1[1] < pt2[1]) {
                                alpha_1 = Math.PI - omega_1;
                                beta_1 = 3 * Math.PI / 2 + omega_1 / 2;
                                alpha_2 = 3 * Math.PI / 2 + omega_1 / 2;
                                beta_2 = Math.PI - omega_1;
                            } else {
                                alpha_1 = omega_1;
                                beta_1 = Math.PI + omega_1;
                                alpha_2 = Math.PI + omega_1;
                                beta_2 = omega_1;
                            }
                        }
                    }
                    this._context.beginPath();
                    this._context.strokeStyle = "#00FF00";
                    this._context.lineWidth = 4;
                    this._context.arc(pt1[0], pt1[1], this._delta_x / 3 + 5, alpha_1, beta_1, false);
                    this._context.lineTo(pt2[0] + (this._delta_x / 3 + 5) * Math.cos(alpha_2),
                        pt2[1] + (this._delta_x / 3 + 5) * Math.sin(alpha_2));
                    this._context.arc(pt2[0], pt2[1], this._delta_x / 3 + 5, alpha_2, beta_2, false);
                    this._context.lineTo(pt1[0] + (this._delta_x / 3 + 5) * Math.cos(alpha_1),
                        pt1[1] + (this._delta_x / 3 + 5) * Math.sin(alpha_1));
                    this._context.stroke();
                    this._context.closePath();
                }
            }
        }
    }

    _draw_state() {
        const intersections = this._engine.intersections();

        for (let key in intersections) {
            const intersection = intersections[key];
            const pt = this._compute_coordinates(intersection.letter().charCodeAt(0), intersection.number());

            switch (intersection.state()) {
                case Yinsh.State.VACANT:
                    break;
                case Yinsh.State.BLACK_MARKER:
                    this._draw_marker(pt[0], pt[1], Yinsh.Color.BLACK);
                    break;
                case Yinsh.State.WHITE_MARKER:
                    this._draw_marker(pt[0], pt[1], Yinsh.Color.WHITE);
                    break;
                case Yinsh.State.BLACK_MARKER_RING:
                    this._draw_marker(pt[0], pt[1], Yinsh.Color.BLACK);
                    this._draw_ring(pt[0], pt[1], Yinsh.Color.BLACK);
                    break;
                case Yinsh.State.BLACK_RING:
                    this._draw_ring(pt[0], pt[1], Yinsh.Color.BLACK);
                    break;
                case Yinsh.State.WHITE_MARKER_RING:
                    this._draw_marker(pt[0], pt[1], Yinsh.Color.WHITE);
                    this._draw_ring(pt[0], pt[1], Yinsh.Color.WHITE);
                    break;
                case Yinsh.State.WHITE_RING:
                    this._draw_ring(pt[0], pt[1], Yinsh.Color.WHITE);
                    break;
                default:
            }
        }
        this._draw_rows();
    }

    _get_click_position(e) {
        let rect = this._canvas.getBoundingClientRect();

        return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);
            const letter = this._compute_letter(pos.x, pos.y);
            const number = this._compute_number(pos.x, pos.y);
            let ok = false;

            if (letter !== 'X' && number !== -1 &&
                this._engine.exist_intersection(letter, number)) {
                if (this._engine.phase() === Yinsh.Phase.PUT_RING &&
                    this._engine.intersection_state(letter, number) === Yinsh.State.VACANT) {
                    this._selected_coordinates = new Yinsh.Coordinates(letter, number);
                    ok = true;
                } else if (this._engine.phase() === Yinsh.Phase.PUT_MARKER &&
                    ((this._engine.intersection_state(letter, number) === Yinsh.State.BLACK_RING &&
                            this._engine.current_color() === Yinsh.Color.BLACK) ||
                        (this._engine.intersection_state(letter, number) === Yinsh.State.WHITE_RING &&
                            this._engine.current_color() === Yinsh.Color.WHITE))) {
                    this._selected_coordinates = new Yinsh.Coordinates(letter, number);
                    this._selected_ring = this._selected_coordinates;
                    ok = true;
                } else if (this._engine.phase() === Yinsh.Phase.MOVE_RING) {
                    if (this._selected_ring.is_valid()) {
                        if (this._engine._verify_moving(this._selected_ring,
                            new Yinsh.Coordinates(letter, number))) {
                            this._selected_coordinates = new Yinsh.Coordinates(letter, number);
                            ok = true;
                        }
                    }
                } else if (this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_AFTER ||
                    this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_BEFORE) {
                    /*                 selected_coordinates = new Yinsh.Coordinates(letter, number);
                     ok = true; */
                } else if ((this._engine.phase() === Yinsh.Phase.REMOVE_RING_AFTER ||
                        this._engine.phase() === Yinsh.Phase.REMOVE_RING_BEFORE) &&
                    ((this._engine.intersection_state(letter, number) === Yinsh.State.BLACK_RING &&
                            this._engine.current_color() === Yinsh.Color.BLACK) ||
                        (this._engine.intersection_state(letter, number) === Yinsh.State.WHITE_RING &&
                            this._engine.current_color() === Yinsh.Color.WHITE))) {
                    this._selected_coordinates = new Yinsh.Coordinates(letter, number);
                    ok = true;
                }
            }
            if (ok) {
                this._manager.play();
            }
        }
    }

    _on_move(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            let pos = this._get_click_position(event);
            let letter = this._compute_letter(pos.x, pos.y);

            if (letter !== 'X') {
                let number = this._compute_number(pos.x, pos.y);

                if (number !== -1) {
                    if (this._compute_pointer(pos.x, pos.y)) {
                        if (this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_AFTER ||
                            this._engine.phase() === Yinsh.Phase.REMOVE_ROWS_BEFORE) {
                            if ((this._engine.current_color() === Yinsh.Color.BLACK && this._engine.intersection_state(letter, number) === Yinsh.State.BLACK_MARKER) ||
                                (this._engine.current_color() === Yinsh.Color.WHITE && this._engine.intersection_state(letter, number) === Yinsh.State.WHITE_MARKER)) {
                                const coordinates = new Yinsh.Coordinates(letter, number);
                                let found = false;

                                for (let index = 0; index < this._selected_row.length && !found; ++index) {
                                    found = this._selected_row[index].letter() === letter && this._selected_row[index].number() === number;
                                }
                                if (!found) {
                                    const rows = this._engine.get_rows(this._engine.current_color());
                                    let ok = false;
                                    let row = null;

                                    if (this._row_index === -1) {
                                        found = false;
                                        for (let index = 0; index < rows.length && !found; ++index) {
                                            row = rows[index];
                                            for (let index2 = 0; index2 < row.length && !found; ++index2) {
                                                found = row[index2].letter() === coordinates.letter() &&
                                                    row[index2].number() === coordinates.number();
                                                if (found) {
                                                    this._row_index = index;
                                                    ok = true;
                                                }
                                            }
                                        }
                                    } else {
                                        row = rows[this._row_index];
                                        found = false;
                                        for (let index2 = 0; index2 < row.length && !found; ++index2) {
                                            found = row[index2].letter() === coordinates.letter() &&
                                                row[index2].number() === coordinates.number();
                                            if (found) {
                                                ok = true;
                                            }
                                        }
                                    }
                                    if (ok) {
                                        this._selected_row.push(coordinates);
                                        if (this._selected_row.length === 5) {
                                            this._manager.play();
                                        }
                                    } else {
                                        this._selected_row = [];
                                        this._row_index = -1;
                                        this._manager.redraw();
                                    }
                                } else {
                                    this._manager.redraw();
                                }
                            } else {
                                this._selected_row = [];
                                this._row_index = -1;
                                this._manager.redraw();
                            }
                        } else {
                            this._manager.redraw();
                        }
                    }
                }
            }
        }
    }

    _show_intersection() {
        if (this._pointerX !== -1 && this._pointerY !== -1) {
            this._context.fillStyle = "#0000ff";
            this._context.strokeStyle = "#0000ff";
            this._context.lineWidth = 1;
            this._context.beginPath();
            this._context.arc(this._pointerX, this._pointerY, 5, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.fill();
            this._context.stroke();
        }
    }
}

export default Gui;
