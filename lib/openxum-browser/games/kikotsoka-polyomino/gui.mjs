"use strict";

import Graphics from '../../graphics/index.mjs';
import KikotsokaPolyomino from '../../../openxum-core/games/kikotsoka-polyomino/index.mjs';
import OpenXum from '../../openxum/gui.mjs';

const Types = {SQUARE: 0, MARBLE: 1};
const square_width = 6;

class Gui extends OpenXum.Gui {
    constructor(c, e, l, g) {
        super(c, e, l, g);
        this._selected_cell = null;
        this._choices = [];
        this._selected_index = -1;
        this._selected_polyomino_index = -1;
        this._last_coordinates = null;
    }

// public methods
    draw() {
        // fond
        this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);

        this._context.lineWidth = 10;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#8B4513";
        Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

        this._draw_grid();
        this._draw_state();
        this._draw_patterns();
        this._draw_polyominos();
        if (this._engine.phase() === KikotsokaPolyomino.Phase.PUT_POLYOMINO) {
            this._draw_pass_button();
        } else {
            if (this._engine.phase() === KikotsokaPolyomino.Phase.CHOICE_PATTERN) {
                this._choices = this._engine._check_patterns();
                this._draw_choices();
            }
        }
    }

    get_move() {
        if (this._engine.phase() === KikotsokaPolyomino.Phase.PUT_POLYOMINO && this._selected_polyomino_index !== -1 && this._selected_cell !== null) {
            const polyomino = this._engine.current_color() === KikotsokaPolyomino.Color.BLACK ? this._engine._black_polyominos[this._selected_polyomino_index] :
                this._engine._white_polyominos[this._selected_polyomino_index];

            return new KikotsokaPolyomino.Move(KikotsokaPolyomino.MoveType.PUT_POLYOMINO, this._engine._color,
                new KikotsokaPolyomino.Coordinates(this._selected_cell.column, this._selected_cell.line),
                [this._selected_polyomino_index, polyomino.rotation()], -1);
        } else if (this._engine.phase() === KikotsokaPolyomino.Phase.CHOICE_PATTERN && this._selected_index !== -1) {
            return new KikotsokaPolyomino.Move(KikotsokaPolyomino.MoveType.CHOICE_PATTERN, this._engine._color, null, null, this._selected_index);
        } else {
            return new KikotsokaPolyomino.Move(KikotsokaPolyomino.MoveType.PASS, this._engine._color, null, null, -1);
        }
    }

    is_animate() {
        return false;
    }

    is_remote() {
        return false;
    }

    move(move, color) {
        this._manager.play();
        // TODO !!!!!
    }

    set_canvas(c) {
        super.set_canvas(c);

        this._height = this._canvas.height;
        this._width = this._canvas.width;
        this._deltaX = (this._width * 0.95 - 100) / this._engine._size;
        this._deltaY = (this._height * 0.95 - 100) / this._engine._size;
        this._offsetX = this._width / 2 - this._deltaX * this._engine._size / 2;
        this._offsetY = this._height / 2 - this._deltaY * this._engine._size / 2;

        this._scaleX = this._height / this._canvas.offsetHeight;
        this._scaleY = this._width / this._canvas.offsetWidth;

        this._canvas.addEventListener("click", (e) => {
            this._on_click(e);
        });
        this._canvas.addEventListener("mousemove", (e) => {
            this._on_move(e);
        });

        this.draw();
    }

    unselect() {
        this._selected_cell = null;
        this._selected_polyomino_index = -1;
        this._last_coordinates = null;
    }

    // private methods
    _compute_coordinates(x, y) {
        if (x >= this._offsetX && x <= this._width - this._offsetX &&
            y >= this._offsetY && y <= this._height - this._offsetY) {
            return {
                column: Math.floor((x - this._offsetX) / this._deltaX),
                line: Math.floor((y - this._offsetY) / this._deltaY)
            };
        } else {
            return {column: -1, line: -1};
        }
    }

    _draw_choices() {
        this._choices.forEach((origin) => {
            const x = this._offsetX + origin.c * this._deltaX;
            const y = this._offsetY + origin.l * this._deltaY;
            const radius = 8;
            const width = this._deltaX * 3;
            const height = this._deltaY * 3;

            this._context.lineWidth = 1;
            this._context.strokeStyle = "#ffff00";
            this._context.fillStyle = "#ffff00";
            this._context.beginPath();
            this._context.arc(x + this._deltaX / 2, y + this._deltaY / 2, this._deltaX * (1.0 / 3 + 1.0 / 10) / 3, 0.0, 2 * Math.PI);
            this._context.closePath();
            this._context.stroke();
            this._context.fill();

            this._context.lineWidth = 4;
            this._context.beginPath();
            this._context.moveTo(x + radius, y);
            this._context.lineTo(x + width - radius, y);
            this._context.quadraticCurveTo(x + width, y, x + width, y + radius);
            this._context.lineTo(x + width, y + height - radius);
            this._context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
            this._context.lineTo(x + radius, y + height);
            this._context.quadraticCurveTo(x, y + height, x, y + height - radius);
            this._context.lineTo(x, y + radius);
            this._context.quadraticCurveTo(x, y, x + radius, y);
            this._context.closePath();
            this._context.stroke();
        });
    }

    _draw_grid() {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        for (let i = 0; i < this._engine._size; ++i) {
            for (let j = 0; j < this._engine._size; ++j) {
                this._context.beginPath();
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.lineTo(this._offsetX + (i + 1) * this._deltaX, this._offsetY + (j + 1) * this._deltaY);
                this._context.lineTo(this._offsetX + i * this._deltaX, this._offsetY + (j + 1) * this._deltaY);
                this._context.moveTo(this._offsetX + i * this._deltaX, this._offsetY + j * this._deltaY);
                this._context.closePath();
                this._context.stroke();
            }
        }
    }

    _draw_button(pos_x, pos_y, text_width, text) {
        const text_height = 23;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#000000";
        this._context.fillStyle = "#757D75";

        this._context.beginPath();
        this._context.moveTo(this._offsetX + pos_x, this._offsetY + pos_y);
        this._context.lineTo(this._offsetX + pos_x + text_width, this._offsetY + pos_y);
        this._context.lineTo(this._offsetX + pos_x + text_width, this._offsetY + pos_y + text_height);
        this._context.lineTo(this._offsetX + pos_x, this._offsetY + pos_y + text_height);
        this._context.lineTo(this._offsetX + pos_x, this._offsetY + pos_y);
        this._context.closePath();
        this._context.fill();
        this._context.stroke();

        this._context.fillStyle = "#ffffff";
        this._context.font = "20px Colibri";
        this._context.textAlign = "center";
        this._context.textBaseline = "top";
        this._context.beginPath();
        this._context.fillText(text, this._offsetX + pos_x + text_width / 2, this._offsetY + pos_y);
        this._context.closePath();
        this._context.fill();
    }

    _draw_pass_button() {
        const text_width = 100;
        const pos_x = (this._width - 2 * this._offsetX - text_width) / 2;

        if (this._engine.current_color() === KikotsokaPolyomino.Color.BLACK) {
            this._draw_button(pos_x, this._height - 2 * this._offsetY + 5, text_width, "Pass");
        } else {
            this._draw_button(pos_x, -28, text_width, "Pass");
        }
    }

    _draw_pattern(x, y, color, pattern) {
        const width = 10;

        this._context.lineWidth = 1;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#ffffff";
        for (let l = 0; l < 3; ++l) {
            for (let c = 0; c < 3; ++c) {
                if (pattern[l][c] === 1) {
                    if (color === KikotsokaPolyomino.Color.BLACK) {
                        this._context.strokeStyle = "#ffffff";
                        this._context.fillStyle = "#000000";
                    } else {
                        this._context.strokeStyle = "#000000";
                        this._context.fillStyle = "#ffffff";
                    }
                    this._context.beginPath();
                    this._context.moveTo(x + c * width, y + l * width);
                    this._context.lineTo(x + (c + 1) * width, y + l * width);
                    this._context.lineTo(x + (c + 1) * width, y + (l + 1) * width);
                    this._context.lineTo(x + c * width, y + (l + 1) * width);
                    this._context.lineTo(x + c * width, y + l * width);
                    this._context.closePath();
                    this._context.fill();
                    this._context.stroke();
                }
            }
        }
    }

    _draw_patterns_with_color(color) {
        const current_level = color === KikotsokaPolyomino.Color.BLACK ? this._engine._black_level : this._engine._white_level;

        if (current_level < 5) {
            const patterns = KikotsokaPolyomino.Patterns[current_level];

            for (let i = 0; i < patterns.length; ++i) {
                if (color === KikotsokaPolyomino.Color.BLACK) {
                    this._draw_pattern(4, this._height - this._offsetY - 5 - 50 * (patterns.length - i), color, patterns[i]);
                } else {
                    this._draw_pattern(4, this._offsetY + 5 + 50 * i, color, patterns[i]);
                }
            }
        }
    }

    _draw_patterns() {
        this._draw_patterns_with_color(KikotsokaPolyomino.Color.BLACK);
        this._draw_patterns_with_color(KikotsokaPolyomino.Color.WHITE);
    }

    _draw_polyomino(x, y, width, color, polyomino, type) {
        this._context.lineWidth = 1;
        this._context.strokeStyle = "#ffffff";
        this._context.fillStyle = "#ffffff";
        for (let l = 0; l < 3; ++l) {
            for (let c = 0; c < 3; ++c) {
                if (polyomino[l][c] !== 0) {
                    if (color === KikotsokaPolyomino.Color.BLACK) {
                        this._context.strokeStyle = "#000000";
                        this._context.fillStyle = "#ff8c00";
                    } else {
                        this._context.strokeStyle = "#000000";
                        this._context.fillStyle = "#00bfff";
                    }
                    this._draw_square(x + c * width, y + l * width, width, true);
                }
                if (polyomino[l][c] === 2) {
                    if (type === Types.SQUARE) {
                        if (color === KikotsokaPolyomino.Color.WHITE) {
                            this._context.strokeStyle = "#000000";
                            this._context.fillStyle = "#000000";
                        } else {
                            this._context.strokeStyle = "#ffffff";
                            this._context.fillStyle = "#ffffff";
                        }
                        this._draw_square(x + c * width + 3 * width / 8, y + l * width + 3 * width / 8, width / 4, true);
                    } else if (type === Types.MARBLE) {
                        Graphics.marble.draw_marble(this._context, x + c * width + width / 2, y + l * width + width / 2, width / 2,
                            color === KikotsokaPolyomino.Color.BLACK ? "white" : "black");
                    }
                } else if (polyomino[l][c] === 3) {
                    if (type === Types.SQUARE) {
                        if (color === KikotsokaPolyomino.Color.WHITE) {
                            this._context.strokeStyle = "#000000";
                            this._context.fillStyle = "#000000";
                        } else {
                            this._context.strokeStyle = "#ffffff";
                            this._context.fillStyle = "#ffffff";
                        }
                        this._draw_square(x + c * width + width / 4, y + l * width + width / 4, width / 2, true);
                    } else if (type === Types.MARBLE) {
                        Graphics.ring.draw_ring(this._context, x + c * width + width / 2, y + l * width + width / 2, 3 * width / 4,
                            color === KikotsokaPolyomino.Color.BLACK ? "white" : "black", "#8B4513");
                    }
                }
            }
        }
    }

    _draw_polyominos() {
        for (let i = 0; i < this._engine._black_polyominos.length; ++i) {
            const delta = (this._offsetY - 3 * square_width) / 2;

            if (this._engine._black_polyominos[i].is_free()) {
                this._draw_polyomino(i * (3 * square_width + 5) + 5, this._height - delta - 5, square_width, KikotsokaPolyomino.Color.WHITE, this._engine._black_polyominos[i].shape(), Types.SQUARE);
                if (this._engine.current_color() === KikotsokaPolyomino.Color.BLACK && this._selected_polyomino_index === i) {
                    this._context.strokeStyle = "#ffff00";
                    this._draw_square(i * (3 * square_width + 5) + 5, this._height - delta - 5, 3 * square_width, false);
                }
            }
        }
        for (let i = 0; i < this._engine._white_polyominos.length; ++i) {
            const delta = (this._offsetY - 3 * square_width) / 2;

            if (this._engine._white_polyominos[i].is_free()) {
                this._draw_polyomino(i * (3 * square_width + 5) + 5, 5, square_width, KikotsokaPolyomino.Color.BLACK, this._engine._white_polyominos[i].shape(), Types.SQUARE);
                if (this._engine.current_color() === KikotsokaPolyomino.Color.WHITE && this._selected_polyomino_index === i) {
                    this._context.strokeStyle = "#ffff00";
                    this._draw_square(i * (3 * square_width + 5) + 5, 5, 3 * square_width, false);

                }
            }
        }
    }

    _draw_square(x, y, width, fill) {
        this._context.beginPath();
        this._context.moveTo(x, y);
        this._context.lineTo(x + width, y);
        this._context.lineTo(x + width, y + width);
        this._context.lineTo(x, y + width);
        this._context.lineTo(x, y);
        this._context.closePath();
        if (fill) {
            this._context.fill();
        }
        this._context.stroke();
    }

    _draw_state() {
        for (let i = 0; i < this._engine._black_polyominos.length; ++i) {
            const polyomino = this._engine._black_polyominos[i];

            if (!polyomino.is_free()) {
                const x = this._offsetX + (polyomino.coordinates().column() - 1) * this._deltaX;
                const y = this._offsetY + (polyomino.coordinates().line() - 1) * this._deltaY;

                this._draw_polyomino(x, y, this._deltaX, KikotsokaPolyomino.Color.WHITE, polyomino.shape(), Types.MARBLE);
            }
        }
        for (let i = 0; i < this._engine._white_polyominos.length; ++i) {
            const polyomino = this._engine._white_polyominos[i];

            if (!polyomino.is_free()) {
                const x = this._offsetX + (polyomino.coordinates().column() - 1) * this._deltaX;
                const y = this._offsetY + (polyomino.coordinates().line() - 1) * this._deltaY;

                this._draw_polyomino(x, y, this._deltaX, KikotsokaPolyomino.Color.BLACK, polyomino.shape(), Types.MARBLE);
            }
        }

        for (let line = 0; line < this._engine._size; ++line) {
            for (let column = 0; column < this._engine._size; ++column) {
                const state = this._engine._board[line][column];
                const x = this._offsetX + column * this._deltaX + this._deltaX / 2;
                const y = this._offsetY + line * this._deltaY + this._deltaY / 2;

                if (state === KikotsokaPolyomino.State.BLACK_BLOCKED || state === KikotsokaPolyomino.State.WHITE_BLOCKED ||
                    state === KikotsokaPolyomino.State.BLACK_SHIDO_BLOCKED || state === KikotsokaPolyomino.State.WHITE_SHIDO_BLOCKED) {
                    this._context.strokeStyle = "#ffff00";
                    this._context.lineWidth = 4;
                    this._context.beginPath();
                    this._context.arc(x, y, this._deltaX * (1.0 / 3 + 1.0 / 10), 0.0, 2 * Math.PI);
                    this._context.closePath();
                    this._context.stroke();
                }
                if (state === KikotsokaPolyomino.State.BLOCKED_IN_BLACK || state === KikotsokaPolyomino.State.BLOCKED_IN_WHITE) {
                    this._context.strokeStyle = "#000000";
                    this._context.fillStyle = "#ff0000";
                    this._context.lineWidth = 1;
                    this._context.beginPath();
                    this._context.arc(x, y, this._deltaX * (1.0 / 10), 0.0, 2 * Math.PI);
                    this._context.closePath();
                    this._context.stroke();
                    this._context.fill();
                }
            }
        }
    }

    _get_click_position(e) {
        const rect = this._canvas.getBoundingClientRect();

        return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
    }

    _on_click(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            const pos = this._get_click_position(event);

            if (pos.y >= this._height - (3 * square_width + 5) || pos.y <= 3 * square_width + 5) {
                if (this._engine.phase() === KikotsokaPolyomino.Phase.PUT_POLYOMINO) {
                    if (this._engine.current_color() === KikotsokaPolyomino.Color.BLACK) {
                        if (pos.y >= this._height - this._offsetY + 3) {
                            const index = Math.floor((pos.x - 5) / (3 * square_width + 5));

                            if (index >= 0 && index < this._engine._black_polyominos.length) {
                                if (this._selected_polyomino_index === index) {
                                    this._engine._black_polyominos[this._selected_polyomino_index].rotate();
                                } else {
                                    this._selected_polyomino_index = index;
                                }
                                this.draw();
                            }
                        }
                    } else {
                        if (pos.y <= this._offsetY) {
                            const index = Math.floor((pos.x - 5) / (3 * square_width + 5));

                            if (index >= 0 && index < this._engine._white_polyominos.length) {
                                if (this._selected_polyomino_index === index) {
                                    this._engine._white_polyominos[this._selected_polyomino_index].rotate();
                                } else {
                                    this._selected_polyomino_index = index;
                                }
                                this.draw();
                            }
                        }
                    }
                }
            } else {
                if ((pos.y >= this._height - this._offsetY || pos.y <= this._offsetY) && pos.x > (this._width - 100) / 2 && pos.x < (this._width - 100) / 2 + 100) {
                    this._manager.play();
                } else {
                    if (this._engine.phase() === KikotsokaPolyomino.Phase.PUT_POLYOMINO) {
                        if (this._last_coordinates !== null && this._selected_polyomino_index !== -1) {
                            this._selected_cell = this._last_coordinates;
                            this._manager.play();
                        }
                    } else if (this._engine.phase() === KikotsokaPolyomino.Phase.CHOICE_PATTERN) {
                        const select = this._compute_coordinates(pos.x, pos.y);
                        let found = false;
                        let i = 0;

                        while (!found && i < this._choices.length) {
                            found = select.column === this._choices[i].c && select.line === this._choices[i].l;
                            if (!found) {
                                ++i;
                            }
                        }
                        if (found) {
                            this._selected_index = i;
                            this._manager.play();
                        }
                    }
                }
            }
        }
    }

    _on_move(event) {
        if (this._engine.current_color() === this._color || this._gui) {
            if (this._engine.phase() === KikotsokaPolyomino.Phase.PUT_POLYOMINO) {
                if (this._selected_polyomino_index !== -1) {
                    const pos = this._get_click_position(event);
                    const coordinates = this._compute_coordinates(pos.x, pos.y);

                    if (coordinates.line === -1 && coordinates.column === -1) {
                        this.draw();
                    } else if (this._last_coordinates === null || this._last_coordinates.line !== coordinates.line || this._last_coordinates.column !== coordinates.column) {
                        const polyomino = this._engine.current_color() === KikotsokaPolyomino.Color.BLACK ? this._engine._black_polyominos[this._selected_polyomino_index] :
                            this._engine._white_polyominos[this._selected_polyomino_index];
                        const first = this._engine.current_color() === KikotsokaPolyomino.Color.BLACK ? this._engine._black_polyomino_number === this._engine._black_polyominos.length :
                            this._engine._white_polyomino_number === this._engine._white_polyominos.length;

                        if (this._engine.is_valid(polyomino, coordinates, first)) {
                            const x = this._offsetX + (coordinates.column - 1) * this._deltaX;
                            const y = this._offsetY + (coordinates.line - 1) * this._deltaY;

                            this.draw();
                            this._draw_polyomino(x, y, this._deltaX,
                                this._engine.current_color() === KikotsokaPolyomino.Color.BLACK ? KikotsokaPolyomino.Color.WHITE : KikotsokaPolyomino.Color.BLACK,
                                polyomino.shape(), Types.MARBLE);
                            this._last_coordinates = coordinates;
                        }
                    }
                }
            }
        }
    }

}

export default Gui;
