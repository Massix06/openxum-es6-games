"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Tzaar from '../../../openxum-core/games/tzaar/index.mjs';

class Manager extends OpenXum.Manager {
    constructor(t, e, g, o, s, w, f) {
        super(t, e, g, o, s, w, f);
        this.that(this);
    }

    build_move() {
        return new Tzaar.Move();
    }

    get_current_color() {
        return this.engine().current_color() === Tzaar.Color.BLACK ? 'Black' : 'White';
    }

    static get_name() {
        return 'tzaar';
    }

    get_winner_color() {
        return this.engine().winner_is() === Tzaar.Color.BLACK ? 'black' : 'white';
    }

    process_move() {
    }
}

export default Manager;
