"use strict";

import Player from '../../../openxum-core/openxum/player.mjs';
import IAHnefatafl from './player.mjs';

class IAHnefataflPlayer extends Player {
    constructor(c, o, e) {
        super(c, o, e);
    }

// public methods
    confirm() {
        return false;
    }

    is_ready() {
        return true;
    }

    is_remote() {
        return false;
    }

    move() {
        return (new IAHnefatafl(this._color, this._engine, 3)).move();
    }

    reinit(e) {
        this._engine = e;
    }
}

export default IAHnefataflPlayer;
