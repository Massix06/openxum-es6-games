"use strict";

import MCTS from '../../generic/mcts_player.mjs';
import Color from "../../../openxum-core/games/kikotsoka/color.mjs";

class MCTSPlayer extends MCTS {
    constructor(c, o, e) {
        super(c, o, e, 100,
            (b) => {
                return b.is_finished() ||
                    (e._color === Color.BLACK ? e._black_level < b._black_level :
                        e._white_level < b._white_level);
            },
            (b) => {
                if (b.is_finished()) {
                    return b.winner_is();
                } else {
                    return e._color;
                }
            }
        );
    }
}

export default MCTSPlayer;
