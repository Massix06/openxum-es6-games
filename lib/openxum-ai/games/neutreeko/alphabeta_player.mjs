"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';
import ThreeInConnectedRow from "../../../openxum-core/games/neutreeko/three_in_connected_row.mjs";

class AlphaBetaPlayer extends AlphaBeta {
    constructor(c, o, e) {
        super(c, o, e, 3, 1000);
        this._a_index = (this.color() === this._engine._pieces[0][0].color()) ? 0 : 1;
        this._o_index = (this._a_index === 0) ? 1 : 0;
    }

    evaluate(e, depth) {
        // If the game is finished, return final score
        if (e.is_finished()) {
            switch (e.winner_is()) {
                case this.color():
                    return this.VICTORY_SCORE;
                case this.opponent_color():
                    return -this.VICTORY_SCORE;
                default:
                    return 0;
            }
        }

        // Else check if row are connected

        let score = 0;
        let up = 50;
        let evaluatorO = new ThreeInConnectedRow(
            e._pieces[this._o_index][0].coordinates(),
            e._pieces[this._o_index][1].coordinates(),
            e._pieces[this._o_index][2].coordinates()
        );
        let evaluatorAI = new ThreeInConnectedRow(
            e._pieces[this._a_index][0].coordinates(),
            e._pieces[this._a_index][1].coordinates(),
            e._pieces[this._a_index][2].coordinates()
        );

        if (evaluatorO.is_connected_row(evaluatorO._v1) || evaluatorO.is_connected_row(evaluatorO._v2)) {
            score -= up;
        }
        if (evaluatorAI.is_connected_row(evaluatorAI._v1) || evaluatorAI.is_connected_row(evaluatorAI._v2)) {
            score += up;
        }

        return score;
    }
}

export default AlphaBetaPlayer;
