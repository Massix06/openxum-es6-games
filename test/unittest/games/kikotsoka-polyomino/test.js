require = require('esm')(module, { mode: 'auto', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;

describe('Polyominos', () => {
  test('Rotate polyomino', () => {
    for (let k = 0; k < OpenXum.KikotsokaPolyomino.Polyominos.length; ++k) {
      const p = new OpenXum.KikotsokaPolyomino.Polyomino(k, [[0, 0, 0], [0, 0, 0], [0, 0, 0]]);

      for (let r = 1; r < OpenXum.KikotsokaPolyomino.Polyominos[k].length; ++r) {
        p.rotate();
        for (let i = 0; i < 3; ++i) {
          for (let j = 0; j < 3; ++j) {
            expect(p.shape()[i][j]).toBe(OpenXum.KikotsokaPolyomino.Polyominos[k][r][i][j]);
          }
        }
      }
    }
  });

  test('Initialize polyominos', () => {
    const e = new OpenXum.KikotsokaPolyomino.Engine(OpenXum.KikotsokaPolyomino.GameType.SMALL, OpenXum.KikotsokaPolyomino.Color.BLACK);
/*
    for (let i = 0; i < e._black_polyominos.length; ++i) {
      console.log(e._black_polyominos[i].to_string());
    }
    for (let i = 0; i < e._white_polyominos.length; ++i) {
      console.log(e._white_polyominos[i].to_string());
    }
*/
    expect(e._black_polyominos.length).toBe(24);
    expect(e._white_polyominos.length).toBe(24);
  });
});
