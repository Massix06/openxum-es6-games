require = require('esm')(module, { mode: 'auto', cjs: true });

const OpenXum = require('../../../../lib/openxum-core/').default;


const color = OpenXum.Yavalax.Color.WHITE;
const moveType = OpenXum.Yavalax.MoveType.PUT_PIECE;
const coordinates = new OpenXum.Yavalax.Coordinates(0,0);
const move = new OpenXum.Yavalax.Move(moveType,color,coordinates);

describe('coordinates', () => {


    test('the first coordinates should to be true, and the second should to be false', () => {
        const validCord = new OpenXum.Yavalax.Coordinates(12,0);
        const invalidCord = new OpenXum.Yavalax.Coordinates(14,0);
        expect(validCord.is_valid()).toBe(true);
        expect(invalidCord.is_valid()).toBe(false);
    });

    test('valid && invalid coordinates to toString', () => {
        const validCord = new OpenXum.Yavalax.Coordinates(12,0);
        const invalidCord = new OpenXum.Yavalax.Coordinates(14,0);
        expect(validCord.to_string()).toBe("Column:0line:12");
        expect(invalidCord.to_string()).toBe('Invalid coordinates');
    });

});

describe('move', () => {

    test('encode',() => {
        const encode = new OpenXum.Yavalax.Move(0,1,new OpenXum.Yavalax.Coordinates(1,1));
        const str = encode.encode();
        expect(str).toBe('PB11');
    });

    test('decode',() => {
        const decode = new OpenXum.Yavalax.Move(0,1,new OpenXum.Yavalax.Coordinates(1,1));
        decode.decode(move.encode());
        expect(decode._type).toBe(moveType);
        expect(decode._color).toBe(color);
        expect(decode._coordinates._line).toBe(0);
        expect(decode._coordinates._column).toBe(0);

    });

    test('returns the string representation of the move', () => {
        const move = new OpenXum.Yavalax.Move(0, 0, new OpenXum.Yavalax.Coordinates(2, 2));
        const str = move.to_string();
        expect(str).toBe('PutW piece at 2 2');
    });

});

describe('Engine', () => {
    test('_put_stone',() => {
        const engine =  new OpenXum.Yavalax.Engine(0,color);
        engine._put_piece(coordinates);

        expect(engine._gameBoard[0][0]).toBe(color);
        expect(engine._white_stones).toBe(83);
        expect(engine._turn).toBe(2);
    });



    test('line_length',() => {
        const engine =  new OpenXum.Yavalax.Engine(0,color);

        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(2,0));

        console.log(engine._gameBoard[0][0]);
        console.log(engine._gameBoard[1][0]);
        console.log(engine._gameBoard[2][0]);

        const cell = new OpenXum.Yavalax.Coordinates(0,0);

        console.log(engine.line_length(cell,0,1));
        //const length = engine.line_length(cell,0,1);
        //expect(length).toBe(3);
    });

    test('returns a list of possible moves', () => {
        const engine = new OpenXum.Yavalax.Engine();
        const moves = engine.get_possible_move_list();
        expect(Array.isArray(moves)).toBe(true);

    });

    test('winner_is',() => {
        const engine =  new OpenXum.Yavalax.Engine(0,color);

        engine._put_piece(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(2,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(3,0));

        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,1));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,2));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,3));

        engine._put_piece(coordinates);
        expect(engine.winner_is()).toBe(color);


        const engineNoWin =  new OpenXum.Yavalax.Engine(0,color);
        engineNoWin._put_piece(new OpenXum.Yavalax.Coordinates(12,4));
        engineNoWin._blue_stones = 0;
        engineNoWin._white_stones = 0;
        expect(engineNoWin.winner_is()).toBe(OpenXum.Yavalax.Color.EQUAL);

    });

    test('check_winning().hasAlignment|alignmentCount|winningCoord',() => {
        const engine =  new OpenXum.Yavalax.Engine(0,color);

        engine._put_piece(new OpenXum.Yavalax.Coordinates(1,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(2,0));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(3,0));

        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,1));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,2));
        engine._put_piece(new OpenXum.Yavalax.Coordinates(0,3));



        engine._put_piece(coordinates);
        expect(engine.check_winning(coordinates).hasAlignment).toBe(true);
        expect(engine.check_winning(coordinates).alignmentCount).toBe(2);

    });


});
