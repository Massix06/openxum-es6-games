"use strict";

const WebSocket = require('ws');

async function waitUntil(condition, time = 100) {
    while (!condition()) {
        await new Promise((resolve) => setTimeout(resolve, time));
    }
}

class RestWebServicePlayer {
    constructor(c, o, e, t, a, p) {
        this._color = c;
        this._opponent_color = o;
        this._engine = e;
        this._player_type = t;
        this._address = a;
        this._port = p;
        this._connected = false;
        this._started = false;
        this._apply_move = false;
        this._next_move = null;
        this._socket = new WebSocket("ws://" + a + ":" + p + "/openxum");
        this._socket.on('message', (data) => {
            const message = JSON.parse(data);

            if (message.action === "connected") {
                this._connected = true;
            } else if (message.action === "started") {
                this._started = true;
            } else if (message.action === "next_move") {
                this._next_move = message.move;
            } else if (message.action === "apply_move") {
                this._apply_move = true;
            }
        });
    }

    close() {
        this._socket.close();
    }

    // public methods
    color() {
        return this._color;
    }

    async move(resolve, reject, move) {
        if (move) {
            this._apply_move = false;
            this._socket.send(JSON.stringify({
                action: "apply_move",
                move: JSON.stringify(move.to_object())
            }));
            await waitUntil(() => this._apply_move);
            resolve(move.to_object());
        } else {
            this._next_move = null;
            this._socket.send(JSON.stringify({
                action: "next_move"
            }));
            await waitUntil(() => this._next_move !== null);
            resolve(this._next_move);
        }
    }

    async start(resolve, reject) {
        await waitUntil(() => this._connected);
        this._socket.send(JSON.stringify({
            action: "start",
            game: this._engine.get_name(),
            player_type: this._player_type,
            type: this._engine.get_type(),
            color: this._engine.current_color(),
            player_color: this._color,
            opponent_color: this._opponent_color,
            login: this._login
        }));
        await waitUntil(() => this._started);
        resolve();
    }
}

exports = module.exports = RestWebServicePlayer;
