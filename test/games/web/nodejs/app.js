const express = require('express');
const path = require('path');
const __lib = path.resolve(__dirname, '../../../../lib');

const app = express();

app.use(express.static(path.join(__dirname, '../static')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use('/lib', express.static(__lib));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
