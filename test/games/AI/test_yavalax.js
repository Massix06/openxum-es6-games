require = require('esm')(module, {mode: 'auto', cjs: true});

const OpenXum = require('../../../lib/openxum-core').default;
const AI = require('../../../lib/openxum-ai').default;

let e = new OpenXum.Yavalax.Engine(OpenXum.Yavalax.GameType.STANDARD, OpenXum.Yavalax.Color.WHITE);
let p1 = new AI.Generic.RandomPlayer(OpenXum.Yavalax.Color.WHITE, OpenXum.Yavalax.Color.BLUE, e);
let p2 = new AI.Generic.RandomPlayer(OpenXum.Yavalax.Color.BLUE, OpenXum.Yavalax.Color.WHITE, e);
let p = p1;
let moves = [];


while (!e.is_finished()) {

    const move = p.move();

    moves.push(move);
    e.move(move);
    console.log(move);

    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === OpenXum.Yavalax.Color.WHITE ? "WHITE" : "BLUE"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}

