require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let black_win = 0;
let white_win = 0;
let black = core.Neutreeko.Color.BLACK;
let white = core.Neutreeko.Color.WHITE;
let e;
let p1;
let p2;

function getNameOfAI(player) {
    if (player === undefined) {
        return "undefined";
    }
    if (player instanceof ai.Specific.Neutreeko.MCTSPlayer) {
        return "MCTS";
    }
    if (player instanceof ai.Specific.Neutreeko.AlphaBetaAI) {
        return "AlphaBetaAI";
    }
    if (player instanceof ai.Specific.Neutreeko.AlphaBetaPlayer) {
        return "AlphaBetaPlayer";
    }
    return "Other";
}

for (let i = 0; i < 20; ++i) {
    e = new core.Neutreeko.Engine(core.Neutreeko.GameType.STANDARD, black);
    p1 = new ai.Specific.Neutreeko.AlphaBetaAI(black, white, e);
    p2 = new ai.Specific.Neutreeko.AlphaBetaPlayer(white, black, e);

    let p = p1;
    while (!e.is_finished()) {
        let startTime = new Date();
        let move = p.move();

        let endTime = new Date();
        if (e.current_color() === black) {
            console.log("Black choice: " + move.formatted_string() + " (" + (endTime - startTime) / 1000 + " s) " + getNameOfAI(p));
        } else {
            console.log("White choice: " + move.formatted_string() + " (" + (endTime - startTime) / 1000 + " s) " + getNameOfAI(p));
        }

        e.move(move);
        p = p === p1 ? p2 : p1;
    }

    if (e.winner_is() === black) {
        console.log("Winner is Black (" + getNameOfAI(p1) + ")");
        black_win++;
    } else {
        console.log("Winner is White (" + getNameOfAI(p2) + ")");
        white_win++;
    }
}

console.log("Black: " + black_win + " wins (" + getNameOfAI(p1) + ")");
console.log("White: " + white_win + " wins (" + getNameOfAI(p2) + ")");
