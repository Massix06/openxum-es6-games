require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let red_win = 0;
let yellow_win = 0;

for (let i = 0; i < 20; ++i) {
    let e = new core.Invers.Engine(core.Invers.GameType.STANDARD, core.Invers.Color.RED);
    let p1 = new ai.Generic.RandomPlayer(core.Invers.Color.RED, core.Invers.Color.YELLOW, e);
    let p2 = new ai.Generic.MCTSPlayer(core.Invers.Color.YELLOW, core.Invers.Color.RED, e);
    let p = p1;

    while (!e.is_finished()) {
        e.move(p.move());
        p = p === p1 ? p2 : p1;
    }

    console.log("Winner is " + (e.winner_is() === core.Invers.Color.RED ? "red" : "yellow"));
    if (e.winner_is() === core.Invers.Color.RED) {
        red_win++;
    } else {
        yellow_win++;
    }
}

console.log("Yellow wins: " + yellow_win);
console.log("Red wins: " + red_win);
