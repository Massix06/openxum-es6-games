require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Pentago.Engine(core.Pentago.GameType.STANDARD, core.Pentago.Color.BLACK);
let p1 = new ai.Generic.RandomPlayer(core.Pentago.Color.BLACK, core.Pentago.Color.WHITE, e);
let p2 = new ai.Generic.RandomPlayer(core.Pentago.Color.WHITE, core.Pentago.Color.BLACK, e);
let p = p1;
let moves = [];

while (!e.is_finished()) {
    let move = p.move();

    /*  if (move.constructor === Array) {
        for (let i = 0; i < move.length; ++i) {
          console.log(move[i].to_string());
        }
      } else {
        console.log(move.to_string());
      } */

    moves.push(move);
    e.move(move);
    p = p === p1 ? p2 : p1;
}

console.log("Winner is " + (e.winner_is() === core.Pentago.Color.BLACK ? "black" : "white"));
for (let index = 0; index < moves.length; ++index) {
    console.log(moves[index].to_string());
}
