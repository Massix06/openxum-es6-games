require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let black_win = 0;
let white_win = 0;

for (let i = 0; i < 1; ++i) {
    let e = new core.Hnefatafl.Engine(core.Hnefatafl.GameType.STANDARD, core.Hnefatafl.Color.BLACK);
    let p1 = new ai.Generic.RandomPlayer(core.Hnefatafl.Color.BLACK, core.Hnefatafl.Color.WHITE, e);
    let p2 = new ai.Specific.Hnefatafl.Player(core.Hnefatafl.Color.WHITE, core.Hnefatafl.Color.BLACK, e);
    let p = p1;

    while (!e.is_finished()) {
        e.move(p.move());
        p = p === p1 ? p2 : p1;
    }

    console.log("Winner is " + (e.winner_is() === core.Hnefatafl.Color.BLACK ? "black" : "white"));
    if (e.winner_is() === core.Hnefatafl.Color.BLACK) {
        black_win++;
    } else {
        white_win++;
    }
}

console.log("Black wins: " + black_win);
console.log("White wins: " + white_win);
