require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Kikotsoka.Engine(core.Kikotsoka.GameType.SMALL, core.Kikotsoka.Color.BLACK);
let p1 = new ai.Specific.Kikotsoka.RandomPlayer(core.Kikotsoka.Color.BLACK, core.Kikotsoka.Color.WHITE, e);
//let p1 = new ai.Specific.Kikotsoka.MCTSPlayer(core.Kikotsoka.Color.BLACK, core.Kikotsoka.Color.WHITE, e);
//let p2 = new ai.Generic.RandomPlayer(core.Kikotsoka.Color.WHITE, core.Kikotsoka.Color.BLACK, e);
let p2 = new ai.Specific.Kikotsoka.MCTSPlayer(core.Kikotsoka.Color.WHITE, core.Kikotsoka.Color.BLACK, e);
let p = p1;
let moves = [];
let index = 0;
let black_level = e._black_level;
let white_level = e._white_level;

while (!e.is_finished()) {
    ++index;
//  console.log("Move n°" + index);
//  console.log("Move number: " + e.get_possible_move_list().length);

    let move = p.move();

    moves.push(move);
    e.move(move);

    console.log(move.to_string());
//  console.log(e.to_string());

    p = p === p1 ? p2 : p1;

    /*  if (black_level !== e._black_level) {
        black_level = e._black_level;
        console.log("NEXT LEVEL: black => " + black_level);
      }
      if (white_level !== e._white_level) {
        white_level = e._white_level;
        console.log("NEXT LEVEL: white => " + white_level);
      } */
}

console.log("Winner is " +
    (e.winner_is() === core.Kikotsoka.Color.BLACK ? "black" :
        (e.winner_is() === core.Kikotsoka.Color.WHITE ? "white" : "nobody")));
console.log("Move number = " + moves.length);
console.log("Black level = " + e._black_level);
console.log("White level = " + e._white_level);
console.log("Black captured pieces = " + e._black_captured_piece_number);
console.log("White captured pieces = " + e._white_captured_piece_number);

let str = "";

for (let index = 0; index < moves.length; ++index) {
    str += moves[index].encode() + ';';
}
console.log(str);
