require = require('esm')(module, {mode: 'auto', cjs: true});

const core = require('../../../lib/openxum-core').default;
const ai = require('../../../lib/openxum-ai').default;

let e = new core.Abande.Engine(core.Abande.GameType.STANDARD, core.Abande.Color.BLACK);
let p1 = new ai.Generic.RandomPlayer(core.Abande.Color.BLACK, core.Abande.Color.WHITE, e);
let p2 = new ai.Specific.Abande.MCTSPlayer(core.Abande.Color.WHITE, core.Abande.Color.BLACK, e);
let win = [];

win[core.Abande.Color.BLACK] = 0;
win[core.Abande.Color.WHITE] = 0;
win[core.Abande.Color.AVAILABLE] = 0;

for (let i = 0; i < 10; i++) {
    let p = p1;

    console.log(i + 1);
    while (!e.is_finished()) {
        let move = p.move();
        e.move(move);
        p = p === p1 ? p2 : p1;
    }
    win[e.winner_is()]++;
    e._reset();
}

console.log(win);
console.log("Random :" + win[core.Abande.Color.BLACK] + " wins");
console.log("MCTS :" + win[core.Abande.Color.WHITE] + " wins");
console.log("draw :" + win[core.Abande.Color.AVAILABLE] + " draw");
win.fill(0);


for (let i = 0; i < 100; i++) {
    let p = p1;
    while (!e.is_finished()) {
        let move = p.move();
        e.move(move);
        p = p === p1 ? p3 : p1;
    }
    win[e.winner_is()]++;
    e._reset();
}

console.log(win);
console.log("Random :" + win[core.Abande.Color.BLACK] + " wins");
console.log("MinMax :" + win[core.Abande.Color.WHITE] + " wins");
console.log("draw :" + win[core.Abande.Color.AVAILABLE] + " draw");
win.fill(0);

for (let i = 0; i < 100; i++) {
    let p = p2;
    while (!e.is_finished()) {
        let move = p.move();
        e.move(move);
        p = p === p2 ? p3 : p2;
    }
    win[e.winner_is()]++;
    e._reset();
}

console.log(win);
console.log("MCTS :" + win[core.Abande.Color.BLACK] + " wins");
console.log("MinMax :" + win[core.Abande.Color.WHITE] + " wins");
console.log("draw :" + win[core.Abande.Color.AVAILABLE] + " draw");
win.fill(0);
